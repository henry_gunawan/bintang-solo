<div class="error-container">
<?php if ($code == "404"):?>
	<div class="well">
		<h1 class="grey lighter smaller">
			<span class="blue bigger-125">
				<i class="ace-icon fa fa-sitemap"></i>
				404
			</span>
			Page Not Found
		</h1>

		<hr>
		<h3 class="lighter smaller">We looked everywhere but we couldn't find it!</h3>

		<div>
			<form class="form-search">
				<span class="input-icon align-middle">
					<i class="ace-icon fa fa-search"></i>

					<input type="text" class="search-query" placeholder="Give it a search...">
				</span>
				<button class="btn btn-sm" type="button">Go!</button>
			</form>

			<div class="space"></div>
			<h4 class="smaller">Try one of the following:</h4>

			<ul class="list-unstyled spaced inline bigger-110 margin-15">
				<li>
					<i class="ace-icon fa fa-hand-o-right blue"></i>
					Re-check the url for typos
				</li>

				<li>
					<i class="ace-icon fa fa-hand-o-right blue"></i>
					Read the faq
				</li>

				<li>
					<i class="ace-icon fa fa-hand-o-right blue"></i>
					Tell us about it
				</li>
			</ul>
		</div>

		<hr>
		<div class="space"></div>

		<div class="center">
			<a href="javascript:history.back()" class="btn btn-grey">
				<i class="ace-icon fa fa-arrow-left"></i>
				Go Back
			</a>

			<a href="<?php echo Yii::app()->baseUrl."/dashboard";?>" class="btn btn-primary">
				<i class="ace-icon fa fa-tachometer"></i>
				Dashboard
			</a>
		</div>
	</div>
<?php elseif($code == "500"):?>
	<div class="well">
		<h1 class="grey lighter smaller">
			<span class="blue bigger-125">
				<i class="ace-icon fa fa-random"></i>
				500
			</span>
			Something Went Wrong!<br/>
		</h1>
		
		<hr>
		
		<h2 class="lighter smaller">
			<b><?php echo CHtml::encode($message); ?></b>
		</h2>
		
		<h3 class="lighter smaller">
			But we are working
			<i class="ace-icon fa fa-wrench icon-animated-wrench bigger-125"></i>
			on it!
		</h3>

		<div class="space"></div>

		<div>
			<h4 class="lighter smaller">Meanwhile, try one of the following:</h4>

			<ul class="list-unstyled spaced inline bigger-110 margin-15">
				<li>
					<i class="ace-icon fa fa-hand-o-right blue"></i>
					Read the faq
				</li>

				<li>
					<i class="ace-icon fa fa-hand-o-right blue"></i>
					Give us more info on how this specific error occurred!
				</li>
			</ul>
		</div>

		<hr>
		<div class="space"></div>

		<div class="center">
			<a href="javascript:history.back()" class="btn btn-grey">
				<i class="ace-icon fa fa-arrow-left"></i>
				Go Back
			</a>

			<a href="#" class="btn btn-primary">
				<i class="ace-icon fa fa-tachometer"></i>
				Dashboard
			</a>
		</div>
	</div>
<?php else:?>
	<h2>Error <?php echo $code; ?></h2>

	<div class="error">
	<?php echo CHtml::encode($message); ?>
	</div>
<?php endif;?>
</div>