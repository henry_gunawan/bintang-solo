<div class="container">
	<section id="content">
		<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array('id' => 'login-form', 'enableClientValidation' => TRUE, 'clientOptions' => array('validateOnSubmit' => TRUE,),));?>
			<h1>Login Form</h1>
			<?php echo $form->errorSummary($model); ?>
			<div>
				<?php echo $form->textField($model, 'username', array('placeholder' => 'Username', 'autocomplete' => 'off', 'class' => 'input-xlarge', 'id' => 'username', 'autofocus' => 'autofocus')); ?>
			</div>
			<div>
				<?php echo $form->passwordField($model, 'password', array('placeholder' => 'Password', 'autocomplete' => 'off', 'id' => 'password', 'class' => 'input-xlarge')); ?>
			</div>
			<div>
				<input type="submit" value="Log in" />
			</div>
		<?php $this->endWidget(); ?>
	</section><!-- content -->
</div><!-- container -->