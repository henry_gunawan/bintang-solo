<?phpclass CurrencyHelper{	private $_locale;	private $_currency;	private $_numberFormatter = null;		protected static $_instance = null; 		private function __construct()	{	}		public function setLocale($locale) 	{		if ($this->_locale != $locale) {			$this->_locale = $locale;			$this->_numberFormatter = new CNumberFormatter($this->_locale);		}	}		public function setCurrency($currency)	{		$this->_currency = $currency;	}		public static function getInstance($locale = 'id', $currency = 'IDR')
	{
		if (null === self::$_instance) {
			self::$_instance = new self();
		}		self::$_instance->setLocale($locale);		self::$_instance->setCurrency($currency);
	
		return self::$_instance;
	}		public static function formatMoney($amount, $locale ='id', $currency='IDR') 	{		return self::getInstance($locale, $currency)->format($amount);	}		public function format($amount)	{		return $this->_numberFormatter->formatCurrency($amount, $this->_currency);	}}?>