<?php

	class GridHelper extends DefaultHelper
	{
		public static function getTemplate()
		{
			$template = "";
			$template .= "<div style='float: left;'>{summary}</div>";
			$template .= "<div style='text-align: right;'>{pager}</div>";
			$template .= "<div style='clear:both'></div>";
			$template .= "\n{items}\n";
			$template .= "<div style='clear:both'></div>";
			$template .= "<div style='text-align: right;'>{pager}</div>";
			
			return $template;
		}
		
		public static function getDashboardTemplate()
		{
			$template = "";
			$template .= "<div style='float: left;'>{summary}</div>";
			$template .= "<div style='text-align: right;'>{pager}</div>";
			$template .= "<div style='clear:both'></div>";
			$template .= "\n{items}\n";
			$template .= "<div style='clear:both'></div>";
			
			return $template;
		}
		
		public static function getListViewTemplate()
		{
			$template = "";
			$template .= "<div style='float: left;'>{summary}</div>";
			$template .= "<div style='clear:both'></div>";
			$template .= "\n{items}\n";
			$template .= "<div style='clear:both'></div>";
			$template .= "<div style='text-align: right;'>{pager}</div>";
			
			return $template;
		}

		public static function getType()
		{
			return 'striped bordered condensed hover';
		}

		public static function getPager()
		{
			return array(
				'header'               => '',
				'cssFile'              => FALSE,
				'maxButtonCount'       => 5,
				'selectedPageCssClass' => 'active',
				'hiddenPageCssClass'   => 'disabled',
				'firstPageCssClass'    => 'previous',
				'lastPageCssClass'     => 'next',
				'firstPageLabel'       => Yii::t('default', 'First'),
				'lastPageLabel'        => Yii::t('default', 'Last'),
				'prevPageLabel'        => Yii::t('default', 'Prev'),
				'nextPageLabel'        => Yii::t('default', 'Next'),
			);
		}
	}