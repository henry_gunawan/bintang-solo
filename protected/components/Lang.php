<?php

/**
 * Accounting represents function for dealing with accountings such as Jurnal.
 */
class Lang {
	
	public static function getErrorMessage($type='', $numChar='')
	{
		$errorMsg = "";
		switch ($type) {
			case '':
				$errorMsg = 'Harap memperbaiki kesalahan input berikut';
				break;
			case 'required':
				$errorMsg = '{attribute} tidak boleh kosong.';
				break;
			case 'max':
				$errorMsg = '{attribute} tidak boleh melebihi '.$numChar.' karakter.';
				break;
			case 'min':
				$errorMsg = '{attribute} tidak boleh kurang dari '.$numChar.' karakter.';
				break;
			case 'repeat':
				$errorMsg = 'Password harus diulang dengan benar';
				break;
			case 'integer':
				$errorMsg = '{attribute} harus berupa angka tanpa koma';
				break;
			case 'numerical':
				$errorMsg = '{attribute} harus berupa angka (boleh dengan koma)';
				break;
			case 'unique':
				$errorMsg = '{attribute} tersebut sudah terpakai';
		}
		return $errorMsg;
	}
}