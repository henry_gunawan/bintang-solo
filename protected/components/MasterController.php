<?php
class MasterController extends Controller
{
	public $auth_manager; // To be used with Rights

	public $page_caption = '';
	public $page_title = '';
	public $site_name = 'Bing Accessories';
	public $toolbar = array();

	public function accessRules()
	{
		return array(
			array('deny', // deny all guest
				'users' => array('?'),
			),
		);
	}

	public function filters()
	{
		return array(
			//'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
			'rightsAccessControl', // access control with Rights
			//'notificationSeen', //
		);
	}

	public function init()
	{
		$this->auth_manager = Yii::app()->authManager; // To be used with Rights
	}

	public function setCustomPageTitle($title = '')
	{
		$prefix = $this->site_name;

		if (trim($title) == '')
			$title = $prefix;
		else {
			if (count($prefix) >= 1)
				$title = $prefix . ' - ' . $title;
		}
		$this->pageTitle = $title;
	}

	public function filterRightsAccessControl($filter_chain)
	{
		if (!Yii::app()->user->isGuest) {
			$module    = ($this->module) ? $this->module->id . '.' : '';
			$operation = $module . $this->id . '.' . $this->action->id;
			if (Yii::app()->user->checkAccess('System Administrator') || Yii::app()->user->checkAccess('Super Administrator') || Yii::app()->user->checkAccess(strtolower($operation))) {
				$filter_chain->run();
			} else {
				Yii::app()->user->setFlash('error', Yii::t('admin', 'Sorry, you have no sufficient privilege to view the intended page.'));
				$this->redirect(array('/dashboard'));
			}
		} else {
			Yii::app()->user->setFlash('error', Yii::t('admin', 'Please login first.'));
			//$this->redirect(array('/admin/default/index'));
			Yii::app()->user->loginRequired();
		}
	}

	function actionGeneratemethods()
	{
		// Change the following variables to adapt your class
		$desc = 'Description Text';

		// No need to change the following variables.
		$class_name         = get_class($this);
		$class_methods      = get_class_methods($class_name);
		$controller_name    = str_replace('Controller', '', $class_name);
		$module_name        = (isset($this->module) ? $this->module->getName() : 'admin');
		$current_controller = $this->ID;
		$prefix_z           = $module_name . '.' . $current_controller . '.';

		$prefix  = "INSERT IGNORE INTO `tbl_auth_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES('";
		$postfix = "', 0, $desc, '', 'N;');";

		foreach ($class_methods as $x) {
			$y = substr($x, 0, 6);
			$z = substr($x, 6);
			$z = strtolower($z);

			$desc    = "'" . ucwords($z) . " " . $controller_name . "'";
			$postfix = "', 0, $desc, '', 'N;');";

			if (($y == 'action') && ($x != 'actions') && ($z != 'generatemethods')) {
				$z = $prefix_z . $z;
				echo $prefix . $z . $postfix . '<br />';
			}
		}
	}
}