<?php

	// this file must be stored in:
	// protected/components/WebUser.php

	class WebUser extends CWebUser
	{
		const USER_ADMIN    = 'admin';
		const USER_CUSTOMER = 'customer';

		// Store model to not repeat query.
		private $_model;

		// Return first name.
		// access it by Yii::app()->user->first_name
		public function getUserID()
		{
			return (isset(Yii::app()->user->id) ? Yii::app()->user->id : 0);
		}

		public function getUsername()
		{
			$user = $this->loadUser(Yii::app()->user->id);

			return $user->employee_username;
		}
		
		public function getName()
		{
			$user = $this->loadUser(Yii::app()->user->id);

			return $user->employee_name;
		}

		protected function loadUser($id = NULL)
		{
			if ($this->_model === NULL) {
				if ($id !== NULL)
					$this->_model = Employee::model()->findByPk($id);
			}

			return $this->_model;
		}
		
		/**
		 * This function is used to check wether the logged in user can access certain module/controller/action.
		 * Mostly used in 'visible' attribute in menu
		 * @param string $module - module name in modules folder
		 * @param string $controller - controller name, for example: "article" for ArticleController.php, "customer" for CustomerController.php, etc
		 * @param string $action - actions in the controller, for example: "create" for actionCreate, "update" for actionUpdate
		 * @return boolean $permitted
		 */
		public function mayAccess($module, $controller, $action)
		{
			$permitted = FALSE;
			if (!Yii::app()->user->isGuest) {
				$operation = str_replace('/', '.', $module) . '.' . $controller . '.' . $action;
				if (Yii::app()->user->checkAccess('System Administrator') || Yii::app()->user->checkAccess('Super Administrator') || Yii::app()->user->checkAccess(strtolower($operation))) {
					$permitted = TRUE;
				}
			}

			return $permitted;
		}
		
		/**
		 * Check whether current User is a Super Administrator
		 * @return boolean TRUE if current User is a Super Administrator
		 */
		public function isSuperAdmin()
		{
			return $this->checkAccess('Super Administrator');
		}
		
		public function isSystemAdmin()
		{
			return $this->checkAccess('System Administrator');
		}
		
		/**
		 * Check whether current User has access to a specified Operation
		 * @param string $operation Name of the Operation
		 * @return boolean TRUE if current User has access to the Operation; FALSE otherwise
		 */
		public function hasAccess($module, $controller, $action)
		{
			$is_permitted = false;
			// If Operation is specified
			if ($controller && $action)
			{
				$module = str_replace('/', '.', $module);
				$operation = $module . '.' . $controller . '.' . $action;
				// If current User is an Admin or has access to the Operation
				if ($this->isSystemAdmin() || $this->isSuperAdmin() || $this->checkAccess(strtolower($operation)))
				{
					$is_permitted = true;
				}
				//var_dump($operation);
			}
			return $is_permitted;
		}
	}

?>