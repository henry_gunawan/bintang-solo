<?php
	class SettingHelper extends DefaultHelper
	{
		public static function getValue($settingName)
		{
			$setting = Setting::model()->find('setting_name=:name', array(":name" => $settingName));
			if ($setting)
				return $setting->setting_value;
			return "";
		}
	}
?>