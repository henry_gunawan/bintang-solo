<?php

	class DateHelper extends DefaultHelper
	{
		/*
		 * Returns a human-friendly date/datetime format.
		 *
		 * @param   date    $date Set selected date/datetime
		 * @param   boolean $is_long Whether to display long date/datetime format or not.
		 * @param   boolean $with_time Whether to display the datetime or only the date.
		 *
		 * @return  string  Human-Friendly date/datetime format.
		 */

		public static function getHumanizedDate($date = NULL, $is_long = TRUE, $with_time = FALSE, $remove_year = FALSE, $remove_date = FALSE, $pm_am = TRUE)
		{
			if ($date == NULL) {
				$date = date('Y-m-d H:i:s');
			}

			if ($is_long === TRUE) {
				if ($with_time === TRUE) {
					if ($pm_am) {
						return date('d M Y, g:i A', strtotime($date));
					} else {
						return date('d F Y H:i:s', strtotime($date));
					}
				} else {
					$format = 'd F Y';
					if ($remove_year == TRUE) {
						$format = 'd F';
					} else {
						if ($remove_date) {
							$format = 'F Y';
						}
					}

					return date($format, strtotime($date));
				}
			} else {
				if ($with_time === TRUE) {
					if ($pm_am) {
						return date('d M Y, g:i A', strtotime($date));
					} else {
						return date('d F Y H:i:s', strtotime($date));
					}
				} else {
					$format = 'd-m-Y';
					if ($remove_year == TRUE) {
						$format = 'd-m';
					} else {
						if ($remove_date) {
							$format = 'm-Y';
						}
					}

					return date($format, strtotime($date));
				}
			}

			return $date;
		}

		/*
		 * Returns a human-friendly datetime format.
		 * Please take note that this function is extending getHumanizedDate function.
		 *
		 * @param   date    $date Set selected datetime
		 * @param   boolean $is_long Whether to display long datetime format or not.
		 * @param   boolean $with_time Whether to display the datetime or only the date.
		 *
		 * @return  string  Human-Friendly datetime format.
		 */

		public static function getHumanizedDateTime($date = NULL, $is_long = TRUE)
		{
			return self::getHumanizedDate($date, $is_long, TRUE);
		}

		/*
		 * Converts a local Unix timestamp to GMT
		 *
		 * @param	integer     Unix timestamp
		 * @param    bool       Whether to return the value as datetime or integer
		 *
		 * @return   integer
		 * @return   string
		 */

		public static function getGmtFromLocal($time = '', $return_as_datetime = TRUE, $return_date_only = FALSE)
		{
			if ($time == '') {
				$time = strtotime(date('Y-m-d H:i:s'));
			}

			$timezone = ConfigurationHelper::getTimezone();
			$time -= self::getTimezones($timezone) * 3600;

			$dst = FALSE;
			if (isset(Yii::app()->params['timezone_dst'])) {
				$dst = Yii::app()->params['timezone_dst'];
			}

			if ($dst == TRUE) {
				$time -= 3600;
			}

			if ($return_as_datetime) {
				return date('Y-m-d H:i:s', $time);
			} else {
				if ($return_date_only) {
					return date('Y-m-d', $time);
				}
			}

			return $time;
		}

		/*
		 * Converts GMT time to a localized value.
		 *
		 * Takes a Unix timestamp (in GMT) as input, and returns
		 * at the local value based on the timezone and DST setting
		 * submitted.
		 *
		 * @param    integer    Unix timestamp
		 * @param    string     Timezone
		 * @param    bool       Whether DST is active
		 * @param    bool       Whether to return the value as datetime or integer
		 *
		 * @return   integer
		 * @return   string
		 */

		public static function getLocalFromGmt($time = '', $timezone = '', $dst = FALSE, $return_as_datetime = TRUE)
		{
			if ($timezone == '') {
				$timezone = ConfigurationHelper::getTimezone();
			}
			if ($time == '') {
				return time();
			}

			$time += self::getTimezones($timezone) * 3600;

			if ($dst == TRUE) {
				$time += 3600;
			}

			if ($return_as_datetime) {
				return date('Y-m-d H:i:s', $time);
			}

			return $time;
		}

		public static function getTimezonesData()
		{
			return array(
				'UM12' => '(UTC - 12:00) Enitwetok, Kwajalien',
				'UM11' => '(UTC - 11:00) Nome, Midway Island, Samoa',
				'UM10' => '(UTC - 10:00) Hawaii',
				'UM9'  => '(UTC - 9:00) Alaska',
				'UM8'  => '(UTC - 8:00) Pacific Time',
				'UM7'  => '(UTC - 7:00) Mountain Time',
				'UM6'  => '(UTC - 6:00) Central Time, Mexico City',
				'UM5'  => '(UTC - 5:00) Eastern Time, Bogota, Lima, Quito',
				'UM4'  => '(UTC - 4:00) Atlantic Time, Caracas, La Paz',
				'UM25' => '(UTC - 3:30) Newfoundland',
				'UM3'  => '(UTC - 3:00) Brazil, Buenos Aires, Georgetown, Falkland Is.',
				'UM2'  => '(UTC - 2:00) Mid-Atlantic, Ascention Is., St Helena',
				'UM1'  => '(UTC - 1:00) Azores, Cape Verde Islands',
				'UTC'  => '(UTC) Casablanca, Dublin, Edinburgh, London, Lisbon, Monrovia',
				'UP1'  => '(UTC + 1:00) Berlin, Brussels, Copenhagen, Madrid, Paris, Rome',
				'UP2'  => '(UTC + 2:00) Kaliningrad, South Africa, Warsaw',
				'UP3'  => '(UTC + 3:00) Baghdad, Riyadh, Moscow, Nairobi',
				'UP25' => '(UTC + 3:30) Tehran',
				'UP4'  => '(UTC + 4:00) Adu Dhabi, Baku, Muscat, Tbilisi',
				'UP35' => '(UTC + 4:30) Kabul',
				'UP5'  => '(UTC + 5:00) Islamabad, Karachi, Tashkent',
				'UP45' => '(UTC + 5:30) Bombay, Calcutta, Madras, New Delhi',
				'UP6'  => '(UTC + 6:00) Almaty, Colomba, Dhaka',
				'UP7'  => '(UTC + 7:00) Bangkok, Hanoi, Jakarta',
				'UP8'  => '(UTC + 8:00) Beijing, Hong Kong, Perth, Singapore, Taipei',
				'UP9'  => '(UTC + 9:00) Osaka, Sapporo, Seoul, Tokyo, Yakutsk',
				'UP85' => '(UTC + 9:30) Adelaide, Darwin',
				'UP10' => '(UTC + 10:00) Melbourne, Papua New Guinea, Sydney, Vladivostok',
				'UP11' => '(UTC + 11:00) Magadan, New Caledonia, Solomon Islands',
				'UP12' => '(UTC + 12:00) Auckland, Wellington, Fiji, Marshall Island',
			);
		}

		public static function getTimezones($tz = '')
		{
			$zones = array(
				'UM12'   => -12,
				'UM11'   => -11,
				'UM10'   => -10,
				'UM95'   => -9.5,
				'UM9'    => -9,
				'UM8'    => -8,
				'UM7'    => -7,
				'UM6'    => -6,
				'UM5'    => -5,
				'UM45'   => -4.5,
				'UM4'    => -4,
				'UM35'   => -3.5,
				'UM3'    => -3,
				'UM2'    => -2,
				'UM1'    => -1,
				'UTC'    => 0,
				'UP1'    => +1,
				'UP2'    => +2,
				'UP3'    => +3,
				'UP35'   => +3.5,
				'UP4'    => +4,
				'UP45'   => +4.5,
				'UP5'    => +5,
				'UP55'   => +5.5,
				'UP575'  => +5.75,
				'UP6'    => +6,
				'UP65'   => +6.5,
				'UP7'    => +7,
				'UP8'    => +8,
				'UP875'  => +8.75,
				'UP9'    => +9,
				'UP95'   => +9.5,
				'UP10'   => +10,
				'UP105'  => +10.5,
				'UP11'   => +11,
				'UP115'  => +11.5,
				'UP12'   => +12,
				'UP1275' => +12.75,
				'UP13'   => +13,
				'UP14'   => +14
			);

			if ($tz == '') {
				return $zones;
			}

			if ($tz == 'GMT')
				$tz = 'UTC';

			return (!isset($zones[$tz])) ? 0 : $zones[$tz];
		}

		/*
		 * return an array of time for a Time dropdown. For example: array('00:00' => '00:00', '00:30' => '00:30', '01:00' => '01:00', '01:30' => '01:30')
		 * @param
		 * $step -- interval of time, default 30 minutes.
		 * $timestart
		 * $timefinish
		 */

		public static function makeTimePicker($step = 30, $timestart = 0, $timefinish = 24)
		{
			$hourctr   = $timestart;
			$minctr    = 0;
			$timearray = array();
			while ($hourctr < $timefinish) {
				$hourpadding   = $hourctr < 10 ? '0' : '';
				$minutepadding = $minctr < 10 ? '0' : '';

				$timearray[$hourpadding . $hourctr . ':' . $minutepadding . $minctr] = $hourpadding . $hourctr . ':' . $minutepadding . $minctr;

				$minctr += $step;
				if ($minctr >= 60) {
					$minctr -= 60;
					$hourctr += 1;
				}
			}
			$timearray['23:59'] = '23:59';

			return $timearray;
		}
		
		public static function makeHourPicker($timestart = 0, $timefinish = 24)
		{
			$hourctr   = $timestart;
			$timearray = array();
			while ($hourctr < $timefinish) {
				$hourpadding   = $hourctr < 10 ? '0' : '';
				$timearray[$hourpadding.$hourctr] = $hourpadding .$hourctr;
				$hourctr++;
			}
			return $timearray;
		}
		
		public static function makeMinutePicker($timestart = 0, $timefinish = 60)
		{
			$minutectr   = $timestart;
			$timearray = array();
			while ($minutectr < $timefinish) {
				$minutepadding   = $minutectr < 10 ? '0' : '';
				$timearray[$minutepadding.$minutectr] = $minutepadding .$minutectr;
				$minutectr++;
			}
			return $timearray;
		}

		/*
		 * Add few more seconds/minutes to a datetime.
		 *
		 * @param    datetime   Date Time
		 * @param    seconds    Seconds to add
		 *
		 * @return   datetime
		 */

		public static function getAddSeconds($datetime = NULL, $seconds = 0)
		{
			if ($datetime == NULL) {
				$datetime = self::getGmtFromLocal();
			}

			$selected = strtotime($datetime);
			$result   = $selected + abs($seconds);

			return date('Y-m-d H:i:s', $result);
		}

		public static function getDays($prompt = '')
		{
			$days = array();
			if (!empty($prompt)) {
				$days[0] = $prompt;
			}
			for ($i = 1; $i <= 31; $i++) {
				$days[$i] = str_pad($i, 2, '0', STR_PAD_LEFT);
			}

			return $days;
		}

		public static function addDays($date_from = '', $number_of_days = 0)
		{
			if ($date_from == '') {
				$date_from = date('Y-m-d');
			}

			return date('Y-m-d', strtotime(date("Y-m-d", strtotime($date_from)) . " +" . $number_of_days . " days"));
		}

		public static function removeDays($date_from = '', $number_of_days = 0)
		{
			if ($date_from == '') {
				$date_from = date('Y-m-d');
			}

			return date('Y-m-d', strtotime(date("Y-m-d", strtotime($date_from)) . " -" . $number_of_days . " days"));
		}

		public static function getMonths($prompt = '')
		{
			$months = array();
			if (!empty($prompt)) {
				$months[0] = $prompt;
			}

			$months[1]  = 'January';
			$months[2]  = 'February';
			$months[3]  = 'March';
			$months[4]  = 'April';
			$months[5]  = 'May';
			$months[6]  = 'June';
			$months[7]  = 'July';
			$months[8]  = 'August';
			$months[9]  = 'September';
			$months[10] = 'October';
			$months[11] = 'November';
			$months[12] = 'December';

			return $months;
		}

		public static function getYears($prompt = '', $min_year = 1900, $max_year = 'this_year', $type = 'asc')
		{
			if (!is_int($min_year)) {
				$min_year = 1900;
			}
			if (($max_year == 'this_year') || (!is_int($max_year))) {
				$max_year = gmdate('Y');
			}


			$years = array();
			if (!empty($prompt)) {
				$years[0] = $prompt;
			}
			if (strtolower(strtoupper($type)) == 'asc') {
				for ($i = $min_year; $i <= $max_year; $i++) {
					$years[$i] = str_pad($i, 4, '0', STR_PAD_LEFT);
				}
			} else {
				for ($i = $max_year; $i >= $min_year; $i--) {
					$years[$i] = str_pad($i, 4, '0', STR_PAD_LEFT);
				}
			}

			return $years;
		}

		public static function displayLocalDate($date)
		{
			$timezone = ConfigurationHelper::getTimezone();
			$edt      = ConfigurationHelper::getDaylightSaving();
			$time     = strtotime($date);

			$date = DateHelper::getLocalFromGmt($time, $timezone, $edt);

			return DateHelper::getHumanizedDateTime($date, TRUE);
		}

		public static function getLocalDate($date)
		{
			$timezone = ConfigurationHelper::getTimezone();
			$edt      = ConfigurationHelper::getDaylightSaving();
			$time     = strtotime($date);

			$date = DateHelper::getLocalFromGmt($time, $timezone, $edt, FALSE);

			return $date;
		}

		public static function calcutateAge($dob = '')
		{
			$_age = floor((self::getGmtFromLocal('', FALSE, FALSE) - strtotime($dob)) / 31556926);

			return $_age;
		}

		public static function convertTimeToTimestamp($time)
		{
			$date = DateTime::createFromFormat('H:i', $time);

			return $date ? $date->getTimestamp() : FALSE;

		}

		public static function quickDateFormat($datetimestring)
		{
			return DateHelper::getHumanizedDate(DateHelper::getLocalFromGmt(strtotime($datetimestring), ConfigurationHelper::getTimezone(), ConfigurationHelper::getDaylightSaving()), TRUE);
		}

		public static function quickDateTimeFormat($datetimestring)
		{
			return DateHelper::getHumanizedDateTime(DateHelper::getLocalFromGmt(strtotime($datetimestring), ConfigurationHelper::getTimezone(), ConfigurationHelper::getDaylightSaving()), TRUE);
		}
	}
