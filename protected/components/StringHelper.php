<?php
	class StringHelper extends DefaultHelper
	{
		/* truncate a long html coded text without stripping the codes */
		public static function csubstr($string, $start, $length = FALSE)
		{
			$pattern = '/(\[\w+[^\]]*?\]|\[\/\w+\]|<\w+[^>]*?>|<\/\w+>)/i';
			$clean   = preg_replace($pattern, chr(1), $string);
			if (!$length)
				$str = substr($clean, $start);
			else {
				$str = substr($clean, $start, $length);
				$str = substr($clean, $start, $length + substr_count($str, chr(1)));
			}
			$pattern = str_replace(chr(1), '(.*?)', preg_quote($str));
			if (preg_match('~' . $pattern . '~is', $string, $matched))
				return strlen($clean) > $length ? $matched[0] . " ... " : $matched[0];

			return $string;
		}
		
		public static function getParams()
		{
			$company                   = ""; //Company::model()->findByPk(1);
			$params['host']            = Yii::app()->baseUrl;
			$params['base_url']        = Yii::app()->baseUrl . '/';
			$params['company_name']    = 'XQ Shop';
			$params['company_logo']    = '';
			$params['now']             = date('Y-m-d H:i:s');
			$params['date']            = date('Y-m-d');
			$params['time']            = date('H:i:s');
			$params['now_e']           = DateHelper::getHumanizedDate();
			$params['date_e']          = DateHelper::getHumanizedDate(NULL, NULL, TRUE);
			$params['time_e']          = date('H:i:s');
			$params['currency_prefix'] = 'S$';
			$params['tc']                = ""; //$company->term_condition;

			$params['company_name'] = 'XQ Shop';
			$params['link_logo']    = '';

			$params['sign_inside']  = '<table border="0" style="width: 100%; margin-top: 20px;"><tbody><tr><td style="width: 70%;">&nbsp;</td><td style="width: 30%;text-align: center; color: #555; font-family: Arial, sans-serif; font-size: 12px; margin: 0; line-height: 20px;"></td></tr></tbody></table>';
			$params['sign_outside'] = '<div style="width: 600px; margin: 0 auto; padding: 40px; color: #999; font-family: Arial, sans-serif; font-size: 12px; line-height: 20px; font-style: italic; text-align: justify;">This email may contain confidential material. If you are not the intended recipient of this email in which you received by mistake, you must delete this email and notify the sender immediately. Any unauthorised usage, dissemination, distribution, printing or copying of this email or any part thereof is strictly prohibited and may lead into legal accountability.</div>';

			return $params;
		}
		
		public static function getParseText($text = '', $params = array())
		{
			$template_str = $text;
			$matches      = array();
			$company      = 'XQ Shop';
			$params_ext = self::getParams();
			$params     = array_merge($params, $params_ext);


			// Payment Request
			$match_total_str = '';
			if (isset($params['row'])) {
				preg_match('/(<tr class="item-row">.*?<\/tr>)/ims', $template_str, $matches);
				if (isset($matches[0])) {
					$match_template = $matches[0];
					foreach ($params['row'] as $row) {
						$match = $match_template;
						foreach ($row as $key => $var) {
							if (!is_array($var))
								$match = str_replace('{row.' . $key . '}', $var, $match);
						}
						$match_total_str .= $match;
					}
				}
			}
			$parsed = $template_str;
			if (count($params)) {
				foreach ($params as $key => $val) {
					if (!is_array($val))
						$parsed = str_replace('{' . $key . '}', $val, $parsed);
				}
			}
			$parsed = preg_replace('/(<tr class="item-row">.*?<\/tr>)/ims', $match_total_str, $parsed);

			preg_match_all('/<!--if 1-->(.*?)<!--\/if-->/ims', $parsed, $matches);
			$search  = $matches[0];
			$replace = $matches[1];
			foreach ($search as $k => $s) {
				$parsed = str_replace($s, $replace[$k], $parsed);
			}
			preg_match_all('/<!--if 0-->(.*?)<!--\/if-->/ims', $parsed, $matches);
			foreach ($matches[0] as $search) {
				$parsed = str_replace($search, '', $parsed);
			}

			// Invoice
			$match_total_str = '';
			if (isset($params['receipt'])) {
				preg_match('/(<tr class="item-receipt">.*?<\/tr>)/ims', $parsed, $matches);
				if (isset($matches[0])) {
					$match_template = $matches[0];
					foreach ($params['receipt'] as $receipt) {
						$match = $match_template;
						foreach ($receipt as $key => $var) {
							if (!is_array($var))
								$match = str_replace('{receipt.' . $key . '}', $var, $match);
						}
						$match_total_str .= $match;
					}
				}
			}
			$parsed = $parsed;
			if (count($params)) {
				foreach ($params as $key => $val) {
					if (!is_array($val))
						$parsed = str_replace('{' . $key . '}', $val, $parsed);
				}
			}
			$parsed = preg_replace('/(<tr class="item-receipt">.*?<\/tr>)/ims', $match_total_str, $parsed);

			preg_match_all('/<!--if 1-->(.*?)<!--\/if-->/ims', $parsed, $matches);
			$search  = $matches[0];
			$replace = $matches[1];
			foreach ($search as $k => $s) {
				$parsed = str_replace($s, $replace[$k], $parsed);
			}
			preg_match_all('/<!--if 0-->(.*?)<!--\/if-->/ims', $parsed, $matches);
			foreach ($matches[0] as $search) {
				$parsed = str_replace($search, '', $parsed);
			}

			return $parsed;
		}
	}
?>