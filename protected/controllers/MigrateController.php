<?php
	class MigrateController extends Controller
	{
		public function actionIndex($param = 'up')
		{
			$this->runMigrationTool($param);
		}

		private function runMigrationTool($param = '')
		{
			$commandPath = Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'commands';
			$runner      = new CConsoleCommandRunner();
			$runner->addCommands($commandPath);
			$commandPath = Yii::getFrameworkPath() . DIRECTORY_SEPARATOR . 'cli' . DIRECTORY_SEPARATOR . 'commands';
			$runner->addCommands($commandPath);
			$args = array('yiic', 'migrate', $param, '--interactive=0');
			ob_start();
			$runner->run($args);
			$data = htmlentities(ob_get_clean(), NULL, Yii::app()->charset);

			echo '<pre>';
			print_r($data);
			echo '</pre>';
		}

		public function actionMailing($mail_to)
		{
			//Yii::import('ext.yii-mail.YiiMailMessage');
			$message = new YiiMailMessage;
			$message->setBody('This is a test email. Sorry for The Inconvenience', 'text/html');
			$message->subject = '.:: 8ttendance : Test Mail ::.';
			if (filter_var($mail_to, FILTER_VALIDATE_EMAIL)) {
				$message->addTo($mail_to);
				$message->from = Yii::app()->params['adminEmail'];
				Yii::app()->mail->send($message);
			} else {
				echo "email format is not valid";
			}
		}
	}