<?php

class SiteController extends Controller
{
	public function filters()
	{
		return array();
	}
	
	public function actionIndex()
	{
		$this->redirect(array('login'));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	public function actionLogin()
	{
		$this->layout = '//layouts/mainlogin';
		
		if (Yii::app()->user->getId()!=''){
			$this->redirect(array('/dashboard'));
		} else {
			$model=new LoginForm;

			// if it is ajax validation request
			if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
	
			// collect user input data
			if(isset($_POST['LoginForm']))
			{
				$model->attributes=$_POST['LoginForm'];
				// validate user input and redirect to the previous page if valid
				if($model->validate() && $model->login()) {
					//$this->redirect(Yii::app()->user->returnUrl);
					$return_url_valid = (int) stripos(Yii::app()->user->returnUrl, 'admin');
					
					$this->redirect($return_url_valid > 0 ? Yii::app()->user->returnUrl : array('/dashboard'));
				}
			}
			// display the login form
			$this->pageTitle = "Staff Login Form";
			$this->render('login',array('model'=>$model));
		}
	}

	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(array('login'));
	}
}