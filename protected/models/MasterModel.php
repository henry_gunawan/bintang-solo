<?php


/**
 * Class to put generic functions for all models
 *
 */
class MasterModel extends CActiveRecord
{
	const SCENARIO_INSERT = 'insert';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_DELETE = 'delete';
	const SCENARIO_REPORT = 'report';

	// Keyword to be used when filling filter for querying records with value null
	const CRITERIA_IS_NULL = ':kosong';

	// Whether or not to fill updated_on and updated_by on create
	protected $alwaysFillUpdateAttributes = false;

	protected function beforeSave()
	{
		if ($this->isNewRecord) {
			if ($this->hasAttribute('created_on')) {
				$this->created_on = new CDbExpression('now()');
			}
			if ($this->hasAttribute('created_by')) {
				$this->created_by = Yii::app()->user->id;
			}
		}
		if (!$this->isNewRecord || $this->alwaysFillUpdateAttributes) {
			if ($this->hasAttribute('updated_on')) {
				$this->updated_on = new CDbExpression('now()');
			}
			if ($this->hasAttribute('updated_by')) {
				$this->updated_by = Yii::app()->user->id;
			}
		}
		return parent::beforeSave();
	}
	
	public function getMaxId($yearMonth, $field) {
        $row = Yii::app()->db->createCommand()
                ->select('max('.$field.') as mid')
                ->where(array('like', $field, '%' . $yearMonth . '%'))
                ->from($this->tableName())
                ->queryRow();
        if ($row['mid']) {
            return $row['mid'];
        } else {
            return 'false';
        }
    }
	
	public function getLatestNumber($code, $field)
	{
		$parts = explode('-', date("d-m-Y"));
		$yearMonth = $parts[2] . $parts[1];
		$latestNumber = "";
        if ($this->getMaxId($yearMonth, $field) == 'false') {
            $latestNumber = $code. $yearMonth . '0001';
		}
		else {
			$id = (int) substr($this->getMaxId($yearMonth, $field), strlen($code)) + 1;
			
			$latestNumber = $code . str_pad($id, 9, 0, STR_PAD_LEFT);
		}
		return $latestNumber;
	}
}