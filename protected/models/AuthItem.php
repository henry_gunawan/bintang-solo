<?php

	/**
	 * This is the model class for table "AuthItem".
	 *
	 * The followings are the available columns in table 'AuthItem':
	 * @property string  $name
	 * @property integer $type
	 * @property string  $description
	 * @property string  $bizrule
	 * @property string  $data
	 *
	 */
	class AuthItem extends CActiveRecord
	{
		const TYPE_OPERATION = 0;
		const TYPE_TASK      = 1;
		const TYPE_ROLE      = 2;

		const ROLE_KEPALA_TOKO = 'Kepala Toko';
		const ROLE_TUKANG_PASANG = 'Tukang Pasang';
		const ROLE_SALES = 'Sales';

		const PAGE_SIZE = 20;

		public $child_entities;
		public $centre_entities;

		/**
		 * Returns the static model of the specified AR class.
		 * @param string $className active record class name.
		 * @return AuthItem the static model class
		 */
		public static function model($className = __CLASS__)
		{
			return parent::model($className);
		}

		/**
		 * @return string the associated database table name
		 */
		public function tableName()
		{
			return 'tbl_auth_item';
		}

		/**
		 * @return array validation rules for model attributes.
		 */
		public function rules()
		{
			// NOTE: you should only define rules for those attributes that
			// will receive user inputs.
			return array(
				array('name', 'required'),
				array('name', 'length', 'max' => 64),
				array('description, bizrule, data, child_entities, centre_entities', 'safe'),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array('name, type, description', 'safe', 'on' => 'search'),
			);
		}

		/**
		 * @return array relational rules.
		 */
		public function relations()
		{
			return array(
				'parent_relations' => array(self::HAS_MANY, 'AuthItemChild', 'child'),
				'parents'          => array(self::HAS_MANY, 'AuthItem', 'child', 'through' => 'parent_relations'),
				'child_relations'  => array(self::HAS_MANY, 'AuthItemChild', 'parent'),
				'children'         => array(self::HAS_MANY, 'AuthItem', 'child', 'through' => 'child_relations'),
				'assignments'      => array(self::HAS_MANY, 'AuthAssignment', 'itemname'),
				'users'            => array(self::HAS_MANY, 'User', 'userid', 'through' => 'assignments'),
				'centres'          => array(self::MANY_MANY, 'Centre', 'auth_centre(auth_name, id_centre)'),
			);
		}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels()
		{
			return array(
				'name'            => 'Nama Peran',
				'type'            => 'Type',
				'description'     => 'Deskripsi',
				'bizrule'         => 'Bizrule',
				'data'            => 'Data',
				'child_entities'  => $this->getChildLabel(),
				'centre_entities' => 'Centres Access List',
			);
		}

		public function defaultScope()
		{
			return array(
				'condition' => 'name <> "System Administrator"',
			);
		}

		protected function getChildLabel()
		{
			$label = 'Fungsi Peran';
			switch ($this->type) {
				case self::TYPE_TASK:
					$label = 'Actions Access List';
					break;
				case self::TYPE_ROLE:
					//$label = 'Modules Access List';
					$label = 'Actions Access List';
					break;
			}

			return $label;
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search()
		{
			$criteria = new CDbCriteria;

			$criteria->compare('name', $this->name, TRUE);
			$criteria->compare('type', $this->type);
			$criteria->compare('description', $this->description, TRUE);

			return new CActiveDataProvider($this, array(
				'criteria'   => $criteria,
				'pagination' => array(
					'pageSize' => Yii::app()->params['listPerPage'],
				),
			));
		}

		public function getChildrenNames()
		{
			$result = 'N/A';
			if ($this->children) {
				$result = '<ul class="auth-children">';
				foreach ($this->children as $child)
					$result .= '<li>' . $child->description . '</li>';
			}
			$result .= '</ul>';

			return $result;
		}

	    public static function loadAuthItems($type = NULL)
	    {
			if($type === NULL)
				$model = AuthItem::model()->findAll();
			else {
				$criteria = new CDbCriteria;
				$criteria->addCondition('type = :type');
				if ($type == self::TYPE_ROLE) {
					$criteria->addCondition('name <> "Super Administrator"');
					$criteria->addCondition('name <> "System Administrator"');
				}
				$criteria->params = array(
					':type' => $type,
				);
				$model = AuthItem::model()->findAll($criteria);
			}
			return $model;
	    }
	}
