<?php

/**
 * This is the model class for table "tbl_employee".
 *
 * The followings are the available columns in table 'tbl_employee':
 * @property integer $employee_id
 * @property string $employee_name
 * @property string $employee_address
 * @property string $employee_phone
 * @property string $employee_email
 * @property string $employee_username
 * @property string $employee_password
 * @property string $employee_note
 * @property string $employee_photo
 * @property string $employee_status
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_on
 * @property string $updated_on
 * @property integer $is_deleted
 *
 * The followings are the available model relations:
 * @property Attendance[] $attendances
 * @property Warehouse[] $warehouses
 */
class Employee extends MasterModel
{
	public $image_file;
	
	public $old_password;
	public $repeat_password;
	public $current_password;
	
	public $role_name;
	public $assigned_roles;
	
	const SCENARIO_EDIT_PROFILE = 'edit_my_profile';
	const SCENARIO_CHANGE_PASS = 'change_password';
	
	const STATUS_ACTIVE = 'active';
	const STATUS_RESIGNED = 'resigned';
	
	public function getStatusLabel($status = NULL) {
        $employeeStatus = array(
            self::STATUS_ACTIVE => 'Aktif',
            self::STATUS_RESIGNED => 'Mengundurkan Diri',
        );

        if ($status === NULL) {
            return $employeeStatus;
        } else {
            return isset($employeeStatus[$status]) ? $employeeStatus[$status] : '';
        }
    }
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_employee';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('employee_username', 'usernameUnique'),
			array('employee_name, employee_username, employee_status', 'required'),
			array('created_by, updated_by, is_deleted', 'numerical', 'integerOnly'=>true),
			array('employee_password', 'required', 'on' => 'insert'),
			array('current_password', 'required', 'on' => self::SCENARIO_CHANGE_PASS),
			array('employee_name', 'length', 'max'=>255),
			array('employee_phone', 'length', 'max'=>20),
			array('employee_email, employee_username, employee_password', 'length', 'max'=>45),
			array('employee_status', 'length', 'max'=>8),
			array('employee_password', 'compare', 'compareAttribute' => 'repeat_password', 'on' => 'insert, '.self::SCENARIO_CHANGE_PASS, 'except' => self::SCENARIO_EDIT_PROFILE, 'message' => Lang::getErrorMessage('repeat')),
			array('old_password', 'compare', 'compareAttribute' => 'current_password', 'on' => 'insert, '.self::SCENARIO_CHANGE_PASS, 'message' => 'Password Sekarang tidak sesuai!'),
			array('employee_address, employee_note, employee_photo, created_on, updated_on', 'safe'),
			array('image_file', 'file', 'types' => 'gif, jpg, jpeg, png', 'maxSize' => 1024 * 1024 * 1024, 'tooLarge' => 'File upload must not exceed 1MB.', 'allowEmpty' => true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('employee_id, employee_name, employee_address, employee_phone, employee_email, employee_username, employee_password, employee_note, employee_photo, employee_status, created_by, updated_by, created_on, updated_on, is_deleted', 'safe', 'on'=>'search'),
		);
	}
	
	public function usernameUnique($attribute, $params)
	{
		if ($attribute == 'employee_username') {
			$employee = Employee::model()->find('employee_username=:username AND employee_id <> :id', array(":username" => $this->$attribute, ":id" => $this->employee_id));
			if ($employee) {
				$this->addError($attribute, 'Username tersebut sudah digunakan oleh karyawan lain.');
			}
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'attendances' => array(self::HAS_MANY, 'Attendance', 'employee_id'),
			'warehouses' => array(self::HAS_MANY, 'Warehouse', 'warehouse_in_charge'),
			'roles' => array(self::MANY_MANY, 'Role', 'tbl_auth_assignment(userid, itemname)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'employee_id' => 'ID',
			'employee_name' => 'Nama',
			'employee_address' => 'Alamat',
			'employee_phone' => 'Telpon',
			'employee_email' => 'Email',
			'employee_username' => 'Username',
			'employee_password' => 'Password',
			'employee_note' => 'Catatan',
			'employee_photo' => 'Foto',
			'image_file' => 'Foto',
			'employee_status' => 'Status',
			'created_by' => 'Created By',
			'updated_by' => 'Updated By',
			'created_on' => 'Created On',
			'updated_on' => 'Updated On',
			'is_deleted' => 'Is Deleted',
			'current_password' => 'Password Sekarang'
		);
	}
	
	protected function afterSave()
	{
		if ($this->employee_id > 1 && in_array($this->scenario, array('create', 'update', 'insert'))) {
			$auth_manager = Yii::app()->authManager;
			$current_user = Yii::app()->user;
			$prev_assign = AuthAssignment::model()->findAllByAttributes(array('userid' => $this->employee_id));
			foreach ($prev_assign as $assign) {
				if ($assign->itemname != 'Super Administrator' || $current_user->isSystemAdmin()) {
					$assignment = $auth_manager->revoke($assign->itemname, $this->employee_id);
				}
			}
			if (!empty($this->assigned_roles)) {
				foreach ($this->assigned_roles as $role) {
					if (empty($role))
						continue;
						
					$auth_item = $auth_manager->getAuthItem($role);
					if ($auth_item && ($current_user->isSystemAdmin() || $role != 'Super Administrator' || ($role == 'Super Administrator' && !$auth_item->isAssigned($this->employee_id)))) {
						$assignment = $auth_manager->assign($role, $this->employee_id);
					}
				}
			}
		}
		parent::afterSave();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('employee_id',$this->employee_id);
		$criteria->compare('employee_name',$this->employee_name,true);
		$criteria->compare('employee_address',$this->employee_address,true);
		$criteria->compare('employee_phone',$this->employee_phone,true);
		$criteria->compare('employee_email',$this->employee_email,true);
		$criteria->compare('employee_username',$this->employee_username,true);
		$criteria->compare('employee_status',$this->employee_status);
		$criteria->compare('is_deleted', '0');
		$criteria->addCondition('employee_id <> "1"');
		
		if ($this->role_name) {
			$criteria->addCondition('t.employee_id IN (SELECT userid FROM tbl_auth_assignment WHERE itemname LIKE :role)');
			$criteria->params[':role'] = $this->role_name;
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'attributes'=>array(
					'role.role_name'=>array(
						'asc'  => 'role.role_name',
						'desc' => 'role.role_name DESC',
					),
					'*',
				),
			),
			'pagination' => array(
				'pageSize' => SettingHelper::getValue('num_of_record_per_page_on_grid')
			)
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Employee the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function validatePassword($password)
	{
	    return md5($password) === $this->employee_password;
	}
	
	/**
	 * This function is to check wether this user has the role or not
	 * @param string $role - role name, for example: "Kepala Toko", "Super Administrator", etc
	 * @return boolean
	 */
	public function hasRole($role)
	{
		$assigned_roles = CHtml::listData($this->roles, 'name', 'name');
		if (in_array($role, $assigned_roles))
			return TRUE;
		else
			return FALSE;
	}

	/**
	 * Checks whether or not the user has access to a given operation
	 * @param string $operation
	 * @return boolean
	 */
	public function mayAccess($operation)
	{
		if (Yii::app()->user->checkAccess('System Administrator') || Yii::app()->user->checkAccess('Super Administrator')) {
			return true;
		}
		
		foreach ($this->roles as $role) {
			$operations = CHtml::listData(AuthItemChild::model()->findAll("parent = '".$role->name."'"), 'child', 'child');
			if (in_array($operation, $operations)) {
				return TRUE;
			}
		}
		return FALSE;
	}
	
	public function loadAuthItems($type = NULL, $exclude_admin = FALSE)
	{
		if ($type === NULL) {
			$model = AuthItem::model()->findAll();
		} else {
			$criteria = new CDbCriteria;
			$criteria->addCondition('type = :type');
			$criteria->params[':type'] = $type;
			if ($exclude_admin) {
				$criteria->addCondition('name <> "Super Administrator"');
				$criteria->addCondition('name <> "System Administrator"');
			}
			$model = AuthItem::model()->findAll($criteria);
		}

		return $model;
	}
	
	public function uploadImage()
	{
		$extension = "jpg";
		if (($pos = strrpos($this->image_file, '.')) !== FALSE)
			$extension = substr($this->image_file, $pos + 1);
		
		$uploadPath = Yii::app()->params['uploadPath'];
		
		// Create directory(s) if it does not exist
		if (!file_exists($uploadPath) and !is_dir($uploadPath)) {
			mkdir($uploadPath, 0777, TRUE);
		}
		
		// Generate image filename from current datetime
		$imageName = strtotime("now");
		$this->employee_photo = $imageName . '.' . $extension;
		
		// Save the original image
		$this->image_file->saveAs($uploadPath . "/" . $this->employee_photo, false);
	}
}
