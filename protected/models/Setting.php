<?php

/**
 * This is the model class for table "tbl_setting".
 *
 * The followings are the available columns in table 'tbl_setting':
 * @property integer $setting_id
 * @property integer $setting_category_id
 * @property string $setting_label
 * @property string $setting_name
 * @property string $setting_value
 * @property string $setting_desc
 * @property string $setting_input_type
 * @property string $setting_input_size
 *
 * The followings are the available model relations:
 * @property SettingCategory $settingCategory
 */
class Setting extends MasterModel
{
	const INPUT_TYPE_TEXT = 'text';
	const INPUT_TYPE_TEXTAREA = 'textarea';
	const INPUT_TYPE_DROPDOWN = 'dropdown';
	const INPUT_TYPE_FILE = 'file';
	
	public $image_file;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_setting';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('setting_category_id, setting_label, setting_name, setting_input_type, setting_input_size', 'required'),
			array('setting_category_id', 'numerical', 'integerOnly'=>true),
			array('setting_label, setting_name, setting_value, setting_desc', 'length', 'max'=>255),
			array('setting_input_type', 'length', 'max'=>8),
			array('setting_input_size', 'length', 'max'=>7),
			array('setting_name', 'unique'),
			array('image_file', 'file', 'types' => 'gif, jpg, jpeg, png', 'allowEmpty' => true, 'maxSize' => 1024 * 1024, 'tooLarge' => 'File upload must not exceed 1MB.'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('setting_id, setting_category_id, setting_label, setting_name, setting_value, setting_desc, setting_input_type, setting_input_size', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'settingCategory' => array(self::BELONGS_TO, 'SettingCategory', 'setting_category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'setting_id' => 'Setting',
			'setting_category_id' => 'Kategori',
			'setting_label' => 'Label',
			'setting_name' => 'Nama',
			'setting_value' => 'Nilai',
			'setting_desc' => 'Deskripsi',
			'setting_input_type' => 'Jenis Masukan',
			'setting_input_size' => 'Ukuran Masukan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('setting_id',$this->setting_id);
		$criteria->compare('setting_category_id',$this->setting_category_id);
		$criteria->compare('setting_label',$this->setting_label,true);
		$criteria->compare('setting_name',$this->setting_name,true);
		$criteria->compare('setting_value',$this->setting_value,true);
		$criteria->compare('setting_desc',$this->setting_desc,true);
		$criteria->compare('setting_input_type',$this->setting_input_type,true);
		$criteria->compare('setting_input_size',$this->setting_input_size,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Setting the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
