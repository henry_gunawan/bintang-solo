<?php

/**
 * This is the model class for table "tbl_payment_bank".
 *
 * The followings are the available columns in table 'tbl_payment_bank':
 * @property integer $payment_bank_id
 * @property string $payment_bank_name
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_on
 * @property string $updated_on
 * @property integer $is_deleted
 *
 * The followings are the available model relations:
 * @property TblSalesPayment[] $tblSalesPayments
 */
class PaymentBank extends MasterModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_payment_bank';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('payment_bank_name', 'required'),
			array('created_by, updated_by, is_deleted', 'numerical', 'integerOnly'=>true),
			array('payment_bank_name', 'length', 'max'=>50),
			array('created_on, updated_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('payment_bank_id, payment_bank_name, created_by, updated_by, created_on, updated_on, is_deleted', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'SalesPayments' => array(self::HAS_MANY, 'SalesPayment', 'payment_bank_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'payment_bank_id' => 'Payment Bank',
			'payment_bank_name' => 'Payment Bank Name',
			'created_by' => 'Created By',
			'updated_by' => 'Updated By',
			'created_on' => 'Created On',
			'updated_on' => 'Updated On',
			'is_deleted' => 'Is Deleted',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('payment_bank_id',$this->payment_bank_id);
		$criteria->compare('payment_bank_name',$this->payment_bank_name,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('updated_on',$this->updated_on,true);
		$criteria->compare('is_deleted',$this->is_deleted);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PaymentBank the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
