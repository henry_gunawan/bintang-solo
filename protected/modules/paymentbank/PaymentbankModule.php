<?php
/**
 * Backup
 * 
 * Yii module to backup, restore databse
 * 
 * @version 1.0
 * @author Shiv Charan Panjeta <shiv@toxsl.com> <shivcharan.panjeta@outlook.com>
 */
class PaymentbankModule extends CWebModule
{
	public $path = null;
	public $layout='//layouts/column2';
	
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'paymentbank.models.*',
			'paymentbank.components.*',
			'paymentbank.assets.js.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			$this->layout = '//layouts/column2';
			return true;
		}
		else
			return false;
	}
}
