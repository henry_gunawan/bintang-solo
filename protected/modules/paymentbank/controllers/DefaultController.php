<?php

class DefaultController extends MasterController
{
	public function init()
	{
		$assetUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias($this->module->id . '.assets'), false, 1, YII_DEBUG);
		$path = $assetUrl . '/js/modules.paymentbank.js';
		Yii::app()->clientScript->registerScriptFile($path);
	}
	
	public function actionCreate()
	{
		$page_caption       = Yii::t('admin', 'Tambah Bank');
		$this->page_caption = $page_caption;
		$this->setCustomPageTitle($page_caption);

		$model = new PaymentBank;
        $model->unsetAttributes();  // clear any default values
		
		if (isset($_POST['PaymentBank'])) {
			$model->attributes = $_POST['PaymentBank'];
			
			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Bank berhasil dibuat!');
				$this->redirect(array('index'));
			}
		}
		
		$this->render('form', array(
            'model' => $model,
        ));
	}
	
	public function actionUpdate($id)
	{
		$page_caption       = Yii::t('admin', 'Update Bank');
		$this->page_caption = $page_caption;
		$this->setCustomPageTitle($page_caption);

		$model = $this->loadModel($id);
		
		if (isset($_POST['PaymentBank'])) {
			$model->attributes = $_POST['PaymentBank'];
			
			if ($model->save()) {
				Yii::app()->user->setFlash('success', 'Bank berhasil diupdate!');
				$this->redirect(array('index'));
			}
		}
		
		$this->render('form', array(
            'model' => $model,
        ));
	}
	
	public function actionDelete($id)
	{
		$model=$this->loadModel($id);
		$model->is_deleted = '1';
		$model->save();
	}
	
	public function actionIndex()
	{
		$page_caption = Yii::t('admin', 'Daftar Bank');
		$this->page_caption = $page_caption;
		$this->setCustomPageTitle($page_caption);

		$model=new PaymentBank;
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['PaymentBank']))
			$model->attributes=$_GET['PaymentBank'];

		$this->render('index',array(
			'model'=>$model,
		));
	}
	
	public function loadModel($id)
	{
		$model = PaymentBank::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Bank tersebut tidak ada');
		return $model;
	}	
}
