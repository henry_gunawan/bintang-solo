<div class="ibox float-e-margins">
    <?php
	$toolbar = array();
	if (Yii::app()->user->hasAccess(Yii::app()->controller->module->id, 'default', 'create'))
		$toolbar[] = CHtml::link('<i class="fa fa-plus"></i> Tambah Bank', array('create'), array('class' => 'btn btn-primary btn-xs'));
	$this->toolbar = $toolbar;

	$this->widget('bootstrap.widgets.TbAlert', array(
		'block' => TRUE,
		'fade' => TRUE,
		'closeText' => '&times;',
		'alerts' => array(
			'success' => array('block' => TRUE, 'fade' => TRUE, 'closeText' => '&times;'),
			'info' => array('block' => TRUE, 'fade' => TRUE, 'closeText' => '&times;'),
			'warning' => array('block' => TRUE, 'fade' => TRUE, 'closeText' => '&times;'),
			'error' => array('block' => TRUE, 'fade' => TRUE, 'closeText' => '&times;'),
		),
			)
	);
	
	$this->widget('bootstrap.widgets.TbGridView', array(
		'id' => 'paymentbank-grid',
		'dataProvider' => $model->search(),
		'filter' => $model,
		'selectableRows' => 0,
		//'template' => GridHelper::getTemplate(),
		'type' => GridHelper::getType(),
		'enablePagination' => TRUE,
		'pager' => GridHelper::getPager(),
		'columns' => array(
			'payment_bank_name',
			
			array(
				'header'   => 'Actions',
				'class'    => 'bootstrap.widgets.TbButtonColumn',
				'template' => '<div class="btn-group">{update}{delete}</div>',
				'buttons'  => array(
					'update' => array(
						'label'   => 'Edit',
						'icon'    => 'fa-pencil',
						'options' => array('class' => 'btn btn-sm btn-warning', 'rel' => ''),
						'visible' => "Yii::app()->user->hasAccess(Yii::app()->controller->module->id, 'default', 'update')",
					),
					'delete' => array(
						'label'   => 'Delete',
						'icon'    => 'fa-trash-o',
						'options' => array('class' => 'btn btn-sm btn-delete btn-danger', 'rel' => ''),
						'visible' => "Yii::app()->user->hasAccess(Yii::app()->controller->module->id, 'default', 'delete')",
					),
				),
				'htmlOptions' => array('style' => 'text-align: right;'),
			),
		),
			)
	);
?>
</div>
	