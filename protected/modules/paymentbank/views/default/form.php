<?php
$toolbar[] = CHtml::link('<i class="fa fa-reply"></i> ' . Yii::t('admin', 'Kembali'), array('index'), array('class' => 'btn btn-xs btn-light'));
$toolbar[] = '<button id="btnSave" href="javascript:;" onclick="submitbutton(\'application.apply\')" type="button" class="btn btn-success btn-xs"><i class="fa fa-save"></i> ' . Yii::t('admin', 'Simpan') . '</button>';
$toolbar[] = '<button id="btnSaveAndStay" name="save_and_stay" href="javascript:;" onclick="submitbutton(\'application.apply\')" type="button" class="btn btn-success btn-xs"><i class="fa fa-save"></i> ' . Yii::t('admin', 'Simpan & Tambah baru') . '</button>';

$this->toolbar = $toolbar;

$this->widget('bootstrap.widgets.TbAlert', array(
    'block' => TRUE,
    'fade' => TRUE,
    'closeText' => '&times;',
    'alerts' => array(
        'success' => array('block' => TRUE, 'fade' => TRUE, 'closeText' => '&times;'),
        'info' => array('block' => TRUE, 'fade' => TRUE, 'closeText' => '&times;'),
        'warning' => array('block' => TRUE, 'fade' => TRUE, 'closeText' => '&times;'),
        'error' => array('block' => TRUE, 'fade' => TRUE, 'closeText' => '&times;'),
    ),
        )
);
?>
<div class="form row-fluid">
    <div class="<?php echo $model->isNewRecord ? '' : 'span12'; ?>">
        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array('id' => 'application_form', 'enableAjaxValidation' => false, 'htmlOptions' => array('enctype' => 'multipart/form-data'))); ?>

        <div class="help-block ptb10"><?php echo Yii::t('admin', 'Kolom dengan tanda <span class="required">*</span> harus diisi.'); ?></div>

        <?php echo $form->errorSummary($model); ?>

        <table border="0" class="table table-condensed table-striped td-middle" style="">
            <tr>
                <td width="15%"><?php echo $form->labelEx($model, 'payment_bank_name'); ?></td>
                <td>
                    <?php echo $form->textField($model, 'payment_bank_name', array('class' => 'input-xlarge')); ?>
                    <?php echo $form->error($model, 'payment_bank_name'); ?>
                </td>
            </tr>
        </table>

        <?php $this->endWidget(); ?>
    </div>
</div><!-- form -->