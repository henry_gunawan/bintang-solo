<?php

class DefaultController extends MasterController
{
	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	public function init()
	{
		$assetUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias($this->module->id . '.assets'), false, 1, YII_DEBUG);
		$path = $assetUrl . '/js/modules.dashboard.js';
		Yii::app()->clientScript->registerScriptFile($path);
	}
	
	public function actionIndex()
	{
		$this->page_caption = "Dashboard";
		$this->setCustomPageTitle($this->page_caption);
                $this->render('index', array());
//		
//		$modelProdWare = new ProductWarehouse;
//		if (isset($_GET['ProductWarehouse']))
//			$modelProdWare->attributes = $_GET['ProductWarehouse'];
//			
//		$modelInvoice = new SalesInvoice;
//		if (isset($_GET['SalesInvoice']))
//			$modelInvoice->attributes = $_GET['SalesInvoice'];
//			
//		$modelPO = new PurchaseOrder;
//		if (isset($_GET['PurchaseOrder']))
//			$modelPO->attributes = $_GET['PurchaseOrder'];
//			
//		$modelCustomer = new Customer;
//		if (isset($_GET['Customer']))
//			$modelCustomer->attributes = $_GET['Customer'];
//			
//		$modelSO = new SalesOrder;
//		if (isset($_GET['SalesOrder']))
//			$modelSO->attributes = $_GET['SalesOrder'];
//			
//		$modelSR = new SalesReturn;
//		if (isset($_GET['SalesReturn']))
//			$modelSR->attributes = $_GET['SalesReturn'];
//		
//		$this->render('index', array(
//			'modelProdWare' => $modelProdWare,
//			'modelInvoice' => $modelInvoice,
//			'modelPO' => $modelPO,
//			'modelCustomer' => $modelCustomer,
//			'modelSO' => $modelSO,
//			'modelSR' => $modelSR,
//		));
	}
}
