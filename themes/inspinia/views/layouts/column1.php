<?php $this->beginContent('//layouts/main'); ?>

<div class="col-lg-12">
    <div class="ibox float-e-margins">
<div class="ibox-content p-md">

                    
        <?php if (isset($this->page_caption) || isset($this->toolbar)): ?>
            <div class="page-header">
                <h1 class=" row header blue">
                    <span class="col-xs-6"><?php echo isset($this->page_caption) ? $this->page_caption : ""; ?></span>
                    <span class="col-xs-6">
                        <label class="pull-right inline">
                            <?php
                            if (isset($this->toolbar)) {
                                foreach ($this->toolbar as $toolbar) {
                                    echo $toolbar . "&nbsp;";
                                }
                            }
                            ?>
                        </label>
                    </span>
                </h1>
            </div>
        <?php endif; ?>
        <!-- PAGE CONTENT BEGINS -->
        <?php echo $content; ?>
    </div>
        
                </div>
    <?php $this->endContent(); ?>