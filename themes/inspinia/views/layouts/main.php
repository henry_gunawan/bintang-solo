<?php
header('Content-Type: text/html; charset=utf-8');
$theme_url = Yii::app()->request->hostInfo . Yii::app()->theme->baseUrl;

// Disable default CSS.
$cs = Yii::app()->clientScript;
$cs->registerCssFile($theme_url . '/css/bootstrap.min.css');
$cs->registerCssFile($theme_url . '/font-awesome/css/font-awesome.min.css');
//page specific plugin styles
$cs->registerCssFile($theme_url . '/css/animate.css');
//core css
$cs->registerCssFile($theme_url . '/css/style.css');
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <script type="text/javascript">
            var base = '<?php echo Yii::app()->request->hostInfo . Yii::app()->createUrl('') . '/'; ?>';
            var theme = '<?php echo $theme_url . '/'; ?>';
            var referer = '<?php echo CHtml::encode(Yii::app()->request->urlReferrer); ?>';
            var baseUrl = '<?php echo Yii::app()->baseUrl; ?>';
        </script>
        <link rel="shortcut icon" href="<?php echo Yii::app()->createUrl('') . '/media/favicon.ico'; ?>"/>

        <title><?php echo CHtml::encode($this->pageTitle); ?></title> 
    </head>

    <body class="pace-done">
        <div id="wrapper">
            <?php echo $this->renderPartial('//default/html/navbar'); ?><!-- NAV BAR -->

            <div id="page-wrapper" class="gray-bg">
                <div class="row border-bottom">
                    <?php echo $this->renderPartial('//default/html/topbar'); ?><!-- TOP BAR -->
                </div>
                <div class="row wrapper border-bottom white-bg page-heading">
                    <?php echo $this->renderPartial('//default/html/breadcrumb'); ?><!-- BREADCRUMB -->
                </div>
                <div class="wrapper wrapper-content animated fadeIn">
                    <div class="row">
                        <?php echo $content; ?>
                    </div>
                </div>
                <div class="footer">
                    <div class="pull-right">
                        <a href="mailto:mr.limcinpao@gmail.com">THE BIG THREE</a>
                    </div>
                    <div>
                        <strong>Copyright</strong> BING ACCESSORIES &copy; 2016
                    </div>
                </div>
            </div>

        </div>

        <?php
        //$cs->registerScriptFile($theme_url . '/js/jquery-1.11.0.min.js');
        Yii::app()->clientScript->registerCoreScript('jquery');
        //$cs->registerScriptFile($theme_url . '/js/jquery-2.1.1.js');
        $cs->registerScriptFile($theme_url . '/js/bootstrap.min.js');
        $cs->registerScriptFile($theme_url . '/js/plugins/metisMenu/jquery.metisMenu.js');
        $cs->registerScriptFile($theme_url . '/js/plugins/slimscroll/jquery.slimscroll.min.js');


        $cs->registerScriptFile($theme_url . '/js/plugins/peity/jquery.peity.min.js');
        $cs->registerScriptFile($theme_url . '/js/inspinia.js');
        $cs->registerScriptFile($theme_url . '/js/plugins/pace/pace.min.js');
        //$cs->registerScriptFile($theme_url . '/js/plugins/rickshaw/vendor/d3.v3.js');
        //$cs->registerScriptFile($theme_url . '/js/plugins/rickshaw/rickshaw.min.js');

        $cs->registerScriptFile($theme_url . '/js/global.js');

        //Yii::app()->clientScript->scriptMap=array('jquery.js'=>false);
        //Yii::app()->clientScript->scriptMap=array('jquery.min.js'=>false);
        ?>
        <!-- Mainly scripts -->



        <script>


            $(document).ready(function () {
                $('.animation_select').click(function () {
                    $('#animation_box').removeAttr('class').attr('class', '');
                    var animation = $(this).attr("data-animation");
                    $('#animation_box').addClass('animated');
                    $('#animation_box').addClass(animation);
                    return false;
                });

//                var graph2 = new Rickshaw.Graph({
//                    element: document.querySelector("#rickshaw_multi"),
//                    renderer: 'area',
//                    stroke: true,
//                    series: [{
//                            data: [{x: 0, y: 2}, {x: 1, y: 5}, {x: 2, y: 8}, {x: 3, y: 4}, {x: 4, y: 8}],
//                            color: '#1ab394',
//                            stroke: '#17997f'
//                        }, {
//                            data: [{x: 0, y: 13}, {x: 1, y: 15}, {x: 2, y: 17}, {x: 3, y: 12}, {x: 4, y: 10}],
//                            color: '#eeeeee',
//                            stroke: '#d7d7d7'
//                        }]
//                });
//                graph2.renderer.unstack = true;
//                graph2.render();
            });
        </script>

    </body>
</html>
