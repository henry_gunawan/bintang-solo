<?php
$currentPage = $this->uniqueid;
$currentController = Yii::app()->controller->id;
$currentFunction = Yii::app()->controller->action->id;
$moduleName = array('');
if (isset(Yii::app()->controller->module->id)) {
    $moduleName = explode('/', Yii::app()->controller->module->id);
}
if (count($moduleName) >= 1) {
    $moduleName = $moduleName[0];
}
?><nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                        <img alt="image" class="img-circle" src="#" />
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="css_animation.html#">
                        <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo Yii::app()->user->name; ?></strong>
                            </span> <span class="text-muted text-xs block">Administrator <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="<?php echo Yii::app()->baseUrl . '/employee/default/myprofile'; ?>">Profile</a></li>
                        <li><a href="<?php echo Yii::app()->baseUrl . '/employee/default/changepassword'; ?>">Ubah Password</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo Yii::app()->baseUrl . "/logout" ?>">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    B+
                </div>
            </li>
            <li>
                <a href=""><i class="fa fa-tachometer"></i> <span class="nav-label">Dashboard</span> </a>                    
            </li>
            <?php
            if (Yii::app()->user->hasAccess('product', 'default', 'index') ||
                    Yii::app()->user->hasAccess('customer', 'default', 'index') ||
                    Yii::app()->user->hasAccess('paymentbank', 'default', 'index') ||
                    Yii::app()->user->hasAccess('expedition', 'default', 'index')):
                ?>

                <li class="<?php echo in_array($moduleName, array('product', 'customer', 'paymentbank', 'expedition')) ? "active" : ""; ?>">
                    <a href="#"><i class="fa fa-desktop"></i> <span class="nav-label">Master</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">Customer</a></li>
                        <li><a href="#">Ekspedisi</a></li>
                        <li><a href="<?php echo Yii::app()->baseUrl . '/paymentbank'; ?>">Bank</a></li>
                        <li><a href="#">Produk</a></li>
                    </ul>                
                </li>
            <?php endif; ?>
            <li>
                <a href="#"><i class="fa fa-list"></i> <span class="nav-label">Inventory</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="#">Inventory</a></li>
                    <li><a href="#">Penyesuaian Stok</a></li>
                </ul>                
            </li>
            <li>
                <a href="#"><i class="fa fa-credit-card"></i> <span class="nav-label">Pembelian</span></a>

            </li>
            <li>
                <a href="#"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Penjualan</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="#">Penjualan</a></li>
                    <li><a href="#">Pembayaran</a></li>
                    <li><a href="#">Pengiriman</a></li>
                </ul>                
            </li>
            <li>
                <a href="#"><i class="fa fa-bar-chart"></i> <span class="nav-label">Laporan</span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="#">Laba/Rugi</a></li>
                    <li><a href="#">Laporan1</a></li>
                    <li><a href="#">Laporan2</a></li>
                    <li><a href="#">Laporan3</a></li>
                    <li><a href="#">Laporan4</a></li>
                    <li><a href="#">Laporan5</a></li>
                    <li><a href="#">Laporan6</a></li>
                    <li><a href="#">Laporan7</a></li>
                </ul>                
            </li>

            <li class="special_link">
                <a href="#"><i class="fa fa-database"></i> <span class="nav-label">Backup</span></a>
                <a href="#"><i class="fa fa-database"></i> <span class="nav-label">Setting</span></a>
                <a href="#"><i class="fa fa-database"></i> <span class="nav-label">Roles</span></a>
            </li>
        </ul>

    </div>
</nav>