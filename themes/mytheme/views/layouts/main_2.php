<?php
	header('Content-Type: text/html; charset=utf-8');
	$theme_url = Yii::app()->request->hostInfo . Yii::app()->theme->baseUrl;

	// Disable default CSS.
	$cs = Yii::app()->clientScript;

	$cs->registerCoreScript('jquery');
	$cs->registerCoreScript('jquery.ui');

	$cs->registerScriptFile($theme_url . '/js/global.js');
	$cs->registerScriptFile($theme_url . '/js/chosen.jquery.min.js');
	$cs->registerScriptFile($theme_url . '/fancybox/jquery.fancybox.js');
	$cs->registerScriptFile($theme_url . '/js/GrowingInput.js');
	$cs->registerScriptFile($theme_url . '/js/jquery.mobilemenu.js');
	$cs->registerScriptFile($theme_url . '/js/date.format.js');

	$cs->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl() . '/jui/css/base/jquery-ui.css');
	$cs->registerCssFile($theme_url . '/css/font-awesome.css');
	$cs->registerCssFile($theme_url . '/css/customfont.css');
	$cs->registerCssFile($theme_url . '/css/style.css');
	$cs->registerCssFile($theme_url . '/css/chosen.css');
	$cs->registerCssFile($theme_url . '/fancybox/jquery.fancybox.css');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta name="language" content="en"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
		<script type="text/javascript">
			var base = '<?php echo Yii::app()->request->hostInfo . Yii::app()->createUrl('') . '/'; ?>';
			var theme = '<?php echo $theme_url . '/'; ?>';
			var referer = '<?php echo CHtml::encode(Yii::app()->request->urlReferrer); ?>';
			var baseUrl = '<?php echo Yii::app()->baseUrl; ?>';
		</script>
		<link rel="shortcut icon" href="<?php echo Yii::app()->createUrl('') . '/media/favicon.ico'; ?>"/>

		<title><?php echo CHtml::encode($this->pageTitle); ?></title>    <!--[if IE 7]>
		<link media="screen" href="<?php echo $theme_url; ?>/css/ie7.css" type="text/css" rel="stylesheet"><![endif]-->    <!--[if IE 8]>
		<link media="screen" href="<?php echo $theme_url; ?>/css/ie8.css" type="text/css" rel="stylesheet"><![endif]-->    <!--[if IE 9]>
		<link media="screen" href="<?php echo $theme_url; ?>/css/ie9.css" type="text/css" rel="stylesheet"><![endif]-->
	</head>

	<body>
		<div class="wrapper">
			<?php
            $current_page = $this->uniqueid;
            $current_controller = Yii::app()->controller->id;
            $current_function = Yii::app()->controller->action->id;
            $module_name = array('');
            if (isset(Yii::app()->controller->module->id)) {
                $module_name = explode('/', Yii::app()->controller->module->id);
            }
				if (count($module_name) >= 1) {
					$module_name = $module_name[0];
				}
				
				$this->widget(
					'bootstrap.widgets.TbNavbar',
					array(
						'type'     => 'inverse',
						'brand'    => 'Garuda',
						'brandUrl' => Yii::app()->baseUrl . '/admin',
						'collapse' => FALSE,
						'items'    => array(
							array(
								'class'       => 'bootstrap.widgets.TbMenu',
								'encodeLabel' => FALSE,
								'htmlOptions' => array('id' => 'header-main-nav'),
								'items'       => array(
									array(
										'label'=>'Dashboard',
										'url'=>array('/dashboard'),
										'icon' => 'iconfa-home',
									),
									array(
										'label'   => 'Master',
										'icon'    => 'iconfa-book',
										'active'  => ($current_page == 'catalog/backend/default' ? TRUE : FALSE),
										'visible' => TRUE,
										'items' => array(
											array(
												'label'   => 'Barang',
												'url'=>array('/product'),
												'active'  => ($current_page == 'product' ? TRUE : FALSE),
												'visible' => TRUE,
											),
											array(
												'label'=>'Customer',
												'visible' => TRUE,
												'url'=>array('/customer'),
												'active'  => ($current_page == 'customer' ? TRUE : FALSE),
											),
											array(
												'label'=>'Supplier',
												'visible' => TRUE,
												'url'=>array('/supplier'),
												'active'  => ($current_page == 'supplier' ? TRUE : FALSE),
											),
											array(
												'label'   => 'Karyawan',
												'url'=>array('/employee'),
												'active'  => ($current_page == 'catalog/backend/default' ? TRUE : FALSE),
												'visible' => TRUE,
											),
											array(
												'label'   => 'Toko',
												'url'=>array('/store'),
												'active'  => ($current_page == 'store' ? TRUE : FALSE),
												'visible' => TRUE,
											),
											array(
												'label'   => 'Gudang',
												'url'=>array('/warehouse'),
												'active'  => ($current_page == 'warehouse' ? TRUE : FALSE),
												'visible' => TRUE,
											),
											array(
												'label'   => 'Ekspedisi',
												'url'=>array('/expedition'),
												'active'  => ($current_page == 'expedition' ? TRUE : FALSE),
												'visible' => TRUE,
											),
										),
									),
									array(
										'label'   => 'Inventory',
										'icon'    => 'iconfa-shopping-cart',
										'active'  => ($current_page == 'dashboard/backend/default' ? TRUE : FALSE),
										'visible' => TRUE,
										'items' => array(
											array(
												'label'=>'Inventory Toko',
												'url'=>array('/store/inventory'),
												'active'  => FALSE,
											),
											array(
												'label'=>'Inventory Gudang',
												'url'=>array('/warehouse/inventory'),
												'active'  => FALSE,
											),
											array(
												'label'=>'DO',
												'url'=>array('/deliveryorder'),
												'active'  => FALSE,
											),
											array(
												'label'=>'Surat Jalan',
												'url'=>array('/traveldoc'),
												'active'  => FALSE,
											),
											array(
												'label'=>'Penyesuaian Stok',
												'url'=>array('/stockadjust'),
												'active'  => FALSE,
											),
										),
									),
									array(
										'label'   => 'Penjualan',
										'icon'    => 'iconfa-shopping-cart',
										'active'  => ($current_page == 'dashboard/backend/default' ? TRUE : FALSE),
										'visible' => TRUE,
										'items' => array(
											array(
												'label'=>'Sales Order',
												'url'=>array('/salesorder'),
												'active'  => FALSE,
											),
											array(
												'label' => 'Nota',
												'url'=>array('/salesinvoice'),
												'active'  => FALSE,
											),
											array(
												'label'=>'Retur Penjualan',
												'url'=>array('/salesreturn'),
												'active'  => FALSE,
											),
											array(
												'label'=>'Pembayaran',
												'url'=>array('/salespayment'),
												'active'  => FALSE,
											),
										),
									),
									array(
										'label'   => 'Pembelian',
										'icon'    => 'iconfa-shopping-cart',
										'active'  => FALSE,
										'visible' => TRUE,
										'items' => array(
											array(
												'label'=>'Purchase Order',
												'url'=>array('/purchaseOrder'),
												'active'  => FALSE,
											),
											array(
												'label'=>'Retur Pembelian',
												'url'=>array('/purchaseReturn'),
												'active'  => FALSE,
											),
//											array(
//												'label'=>'Pembayaran',
//												'url'=>array('/dashboard/underconstruction'),
//												'active'  => FALSE,
//											),
										),
									),
									array(
										'label' => 'Laporan',
										'icon' => 'iconfa-file',
										'active'  => FALSE,
										'visible' => TRUE,
										'items' => array(
											array(
												'label'   => 'Penjualan',
												'active'  => FALSE,
												'visible' => TRUE,
												'items' => array(
													array(
														'label'=>'Laporan Sales Order',
														'url'=>array('/report/sales/salesorder'),
														'active'  => FALSE,
													),
													/*array(
														'label'=>'Laporan Penjualan',
														'url'=>array('/report/sales/salesinvoice'),
														'active'  => FALSE,
													),*/
													array(
														'label'=>'Laporan Retur Penjualan',
														'url'=>array('/report/sales/salesreturn'),
														'active'  => FALSE,
													),
													array(
														'label'=>'Laporan Pembayaran Customer',
														'url'=>array('/report/sales/salespayment'),
														'active'  => FALSE,
													),
													array(
														'label'=>'Laporan Piutang',
														'url'=>array('/report/sales/debts'),
														'active'  => FALSE,
													),
												),
											),
											array(
												'label'   => 'Pembelian',
												'active'  => FALSE,
												'visible' => TRUE,
												'items' => array(
													array(
														'label'=>'Laporan Purchase Order',
														'url'=>array('/report/purchase/purchaseorder'),
														'active'  => FALSE,
													),
													array(
														'label'=>'Laporan Retur Pembelian',
														'url'=>array('/report/purchase/purchasereturn'),
														'active'  => FALSE,
													),
													array(
														'label'=>'Laporan Pembayaran ke Supplier',
														'url'=>array('/report/purchase/purchasepayment'),
														'active'  => FALSE,
													),
													array(
														'label'=>'Laporan Hutang',
														'url'=>array('/report/purchase/debts'),
														'active'  => FALSE,
													),
												),
											),
											array(
												'label'   => 'Inventory',
												'active'  => FALSE,
												'visible' => TRUE,
												'items' => array(
													array(
														'label'=>'Laporan Detil DO',
														'url'=>array('/report/inventory/deliveryorder'),
														'active'  => FALSE,
													),
													array(
														'label'=>'Laporan Kartu Stock (Gudang)',
														'url'=>array('/report/inventory/stockcardwarehouse'),
														'active'  => FALSE,
													),
													array(
														'label'=>'Laporan Kartu Stock (Toko)',
														'url'=>array('/report/inventory/stockcardstore'),
														'active'  => FALSE,
													),
												),
											),
										),
									),
									array(
										'label'   => 'Settings',
										'icon'    => 'iconfa-wrench',
										'active'  => ($current_page == 'dashboard/backend/default' ? TRUE : FALSE),
										'visible' => TRUE,
										'items'   => array(
											array(
												'label'   => 'Setting',
												'active'  => ($current_page == 'dashboard/backend/default' ? TRUE : FALSE),
												'visible' => TRUE,
												'url'=>array('/settings'),
											),
											array(
												'label'   => 'Peran',
												'active'  => ($current_page == 'dashboard/backend/default' ? TRUE : FALSE),
												'visible' => TRUE,
												'url'=>array('/rights/roles'),
											),
										),
									),
								),
							),
							array(
								'class'       => 'bootstrap.widgets.TbMenu',
								'htmlOptions' => array('class' => 'pull-right'),
								'encodeLabel' => FALSE,
								'items'       => array(
									array(
										'label'  => Yii::app()->user->name,
										'url'    => '#',
										'icon'   => 'iconfa-user',
										'items'  => array(
											array('label'  => Yii::t('admin', 'Profil Saya'),
												  'url'=>array('/employee/myprofile'),
												  'active' => ($current_function == 'myprofile' ? TRUE : FALSE)
											),
											array('label'  => Yii::t('admin', 'Ganti Password'),
												  'url'=>array('/employee/changepassword'),
												  'active' => ($current_function == 'changepassword' ? TRUE : FALSE)
											),
											'---',
											array(
												'label'=>'Logout',
												'url'=>array('/logout'),
											),
										),
									),
								),
							)
						)
					)
				);
			?>
			<div class="container-all">
				<?php echo $content; ?>
			</div>
			<div class="push"></div>
		</div>
		<div class="footer navbar-fixed-bottom">
			<div class="navbar-inner footer-inner">
				<div class="copyright">Copyright &copy; 2014 XQ Shop. Developed by <a href="http://www.infinitumsoft.com">InfinitumSoft</a></div>
				<div class="powered-by">Powered By Yii Framework and Twitter Bootstrap</div>
			</div>
		</div>
		<script type="text/javascript">
			(function ($) {
				//init mobile menu combobox
				//$('#header-main-nav').mobileMenu();

				// fix sub nav on scroll
				var $win = $(window), $nav = $('.subhead'), navTop = $('.subhead').length && $('.subhead').offset().top - 40, isFixed = 0;

				processScroll()

				// hack sad times - holdover until rewrite for 2.1
				$nav.on('click', function () {
					if (!isFixed)
						setTimeout(function () {
							$win.scrollTop($win.scrollTop() - 47)
						}, 10)
				})

				$win.on('scroll', processScroll)

				function processScroll() {
					var i, scrollTop = $win.scrollTop();
					if ($win.width() >= 1080) {
						if (scrollTop >= navTop && !isFixed) {
							isFixed = 1;
							$('.subhead_backup').css('height', (parseFloat($nav.height()) + parseFloat(5)) + 'px');
							$nav.addClass('subhead-fixed');
						} else if (scrollTop <= navTop && isFixed) {
							isFixed = 0
							$('.subhead_backup').css('height', '0px');
							$nav.removeClass('subhead-fixed');
						}
					}
				}
			})(jQuery);


            //Attempted fix for bootstrap dropdown menu on touch devices
            $('body').on('touchstart.dropdown', '.dropdown-menu', function (e) { e.stopPropagation(); });
            $('.dropdown-toggle').click(function(e) {
                e.preventDefault();
                setTimeout($.proxy(function() {
                    if ('ontouchstart' in document.documentElement) {
                        $(this).siblings('.dropdown-backdrop').off().remove();
                    }
                }, this), 0);
            });
		</script>

        <?php
        /*$backend_timeout =  ConfigurationHelper::getBackendTimeoutDuration();
        if ($backend_timeout > 0){
            if ($backend_timeout < 300) { $backend_timeout = 300; }
            $this->widget('ext.timeout-dialog.ETimeoutDialog', array(
                'timeout' =>  $backend_timeout,
                'keep_alive_url' => $this->createUrl('/rights/backend/default/keepalive'),
                'logout_redirect_url' => $this->createUrl('/rights/backend/default/logout'),
            ));
        }*/
         ?>

	</body>
</html>