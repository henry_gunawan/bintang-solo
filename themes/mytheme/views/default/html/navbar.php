<?php
	$currentPage = $this->uniqueid;
	$currentController = Yii::app()->controller->id;
	$currentFunction = Yii::app()->controller->action->id;
	$moduleName = array('');
	if (isset(Yii::app()->controller->module->id)) {
		$moduleName = explode('/', Yii::app()->controller->module->id);
	}
	if (count($moduleName) >= 1) {
		$moduleName = $moduleName[0];
	}
?>
<a href="#" class="menu-toggler invisible" id="menu-toggler"></a>

<div id="sidebar" class="sidebar responsive-min sidebar-fixed sidebar-scroll">
    <script type="text/javascript">
        try {
  
            ace.settings.check('sidebar'
        ,  'fix ed' )
        
    } catch (e) {
        }
    </script>

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
			<?php if (Yii::app()->user->hasAccess('backup', 'default', 'index')):?>
            <a href="<?php echo Yii::app()->baseUrl."/backup";?>" class="btn btn-info">
                <i class="ace-icon fa fa-database"></i>
            </a>
			<?php endif;?>
			<?php if (Yii::app()->user->hasAccess('rights', 'roles', 'index')):?>
            <a href="<?php echo Yii::app()->baseUrl."/rights/roles";?>" class="btn btn-warning">
                <i class="ace-icon fa fa-users"></i>
            </a>
			<?php endif;?>
			<?php if (Yii::app()->user->hasAccess('setting', 'default', 'index')):?>
            <a href="<?php echo Yii::app()->baseUrl."/setting";?>" class="btn btn-danger">
                <i class="ace-icon fa fa-cogs"></i>
            </a>
			<?php endif;?>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">

			<?php if (Yii::app()->user->hasAccess('backup', 'default', 'index')):?>
            <span class="btn btn-info"></span>
			<?php endif;?>
			<?php if (Yii::app()->user->hasAccess('rights', 'roles', 'index')):?>
            <span class="btn btn-warning"></span>
			<?php endif;?>
			<?php if (Yii::app()->user->hasAccess('setting', 'default', 'index')):?>
            <span class="btn btn-danger"></span>
			<?php endif;?>
        </div>
    </div><!-- /.sidebar-shortcuts -->

    <ul class="nav nav-list">
        <li class="<?php echo $currentPage == 'dashboard' ? "active" : "";?>">
            <a href="<?php echo  Yii::app()->baseUrl.'/dashboard';?>">
                <i class="menu-icon fa fa-tachometer"></i>
                <span class="menu-text"> Dashboard </span>
            </a>
            <b class="arrow"></b>
        </li>
		<?php if (Yii::app()->user->hasAccess('product', 'category', 'index') ||
					Yii::app()->user->hasAccess('product', 'brand', 'index') ||
					Yii::app()->user->hasAccess('product', 'motif', 'index') ||
					Yii::app()->user->hasAccess('product', 'default', 'index') ||
					Yii::app()->user->hasAccess('customer', 'default', 'index') ||
					Yii::app()->user->hasAccess('supplier', 'default', 'index') ||
					Yii::app()->user->hasAccess('expedition', 'default', 'index') ||
					Yii::app()->user->hasAccess('employee', 'default', 'index')):?>
        <li class="<?php echo in_array($moduleName, array('product', 'customer', 'supplier', 'expedition', 'employee')) ? "active open" : "";?>">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-desktop"></i>
                <span class="menu-text"> Master </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">
				<?php if (Yii::app()->user->hasAccess('product', 'category', 'index') ||
							Yii::app()->user->hasAccess('product', 'brand', 'index') ||
							Yii::app()->user->hasAccess('product', 'motif', 'index') ||
							Yii::app()->user->hasAccess('product', 'default', 'index')):?>
                <li class="<?php echo $moduleName == 'product' ? "active open" : "";?>">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Barang
                        <b class="arrow fa fa-angle-down"></b>
                    </a>
                    <b class="arrow"></b>
                    <ul class="submenu">
						<?php if (Yii::app()->user->hasAccess('product', 'category', 'index')):?>
						<li class="<?php echo $currentPage == 'product/category' ? "active" : "";?>">
                            <a href="<?php echo  Yii::app()->baseUrl.'/product/category';?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Daftar Kategori
                            </a>
                            <b class="arrow"></b>
                        </li>
						<?php endif;?>
						<?php if (Yii::app()->user->hasAccess('product', 'brand', 'index')):?>
                        <li class="<?php echo $currentPage == 'product/brand' ? "active" : "";?>">
                            <a href="<?php echo  Yii::app()->baseUrl.'/product/brand';?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Daftar Merk
                            </a>
                            <b class="arrow"></b>
                        </li>
						<?php endif;?>
						<?php if (Yii::app()->user->hasAccess('product', 'motif', 'index')):?>
						<li class="<?php echo $currentPage == 'product/motif' ? "active" : "";?>">
                            <a href="<?php echo  Yii::app()->baseUrl.'/product/motif';?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Daftar Motif
                            </a>
                            <b class="arrow"></b>
                        </li>
						<?php endif;?>
						<?php if (Yii::app()->user->hasAccess('product', 'default', 'index')):?>
                        <li class="<?php echo $currentPage == 'product/default' ? "active" : "";?>">
							<a href="<?php echo  Yii::app()->baseUrl.'/product';?>">
								<i class="menu-icon fa fa-caret-right"></i>
                                Daftar Barang
                            </a>
                            <b class="arrow"></b>
                        </li>
						<?php endif;?>
                    </ul>
                </li>
				<?php endif;?>
				<?php if (Yii::app()->user->hasAccess('customer', 'default', 'index')):?>
                <li class="<?php echo $moduleName == 'customer' ? "active" : "";?>">
                    <a href="<?php echo  Yii::app()->baseUrl.'/customer';?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Customer
                    </a>
                    <b class="arrow"></b>
                </li>
				<?php endif;?>
				<?php if (Yii::app()->user->hasAccess('supplier', 'default', 'index')):?>
                <li class="<?php echo $moduleName == 'supplier' ? "active" : "";?>">
                    <a href="<?php echo  Yii::app()->baseUrl.'/supplier';?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Supplier
                    </a>
                    <b class="arrow"></b>
                </li>
				<?php endif;?>
				<?php if (Yii::app()->user->hasAccess('expedition', 'default', 'index')):?>
                <li class="<?php echo $moduleName == 'expedition' ? "active" : "";?>">
                    <a href="<?php echo  Yii::app()->baseUrl.'/expedition';?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Ekspedisi
                    </a>
                    <b class="arrow"></b>
                </li>
				<?php endif;?>
				<?php if (Yii::app()->user->hasAccess('employee', 'default', 'index')):?>
                <li class="<?php echo $moduleName == 'employee' ? "active" : "";?>">
                    <a href="<?php echo  Yii::app()->baseUrl.'/employee';?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Karyawan
                    </a>
                    <b class="arrow"></b>
                </li>
				<?php endif;?>
            </ul>
        </li>
		<?php endif;?>
		<?php if (Yii::app()->user->hasAccess('winventory', 'warehouse', 'index') ||
					Yii::app()->user->hasAccess('winventory', 'warehouse', 'inventory') ||
					Yii::app()->user->hasAccess('transfer', 'default', 'index') ||
					Yii::app()->user->hasAccess('winventory', 'stockadjust', 'index')):?>
        <li class="<?php echo in_array($moduleName, array('winventory', 'transfer')) ? "active open" : "";?>">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-list"></i>
                <span class="menu-text"> Inventori </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
				<?php if (Yii::app()->user->hasAccess('winventory', 'warehouse', 'index')):?>
				<li class="<?php echo $currentPage == 'winventory/warehouse' && $currentFunction != 'inventory' ? "active" : "";?>">
                    <a href="<?php echo Yii::app()->baseUrl.'/winventory/warehouse/';?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Daftar Gudang
                    </a>

                    <b class="arrow"></b>
                </li>
				<?php endif;?>
				<?php if (Yii::app()->user->hasAccess('winventory', 'warehouse', 'inventory')):?>
                <li class="<?php echo $currentPage == 'winventory/warehouse' && $currentFunction == 'inventory' ? "active" : "";?>">
                    <a href="<?php echo Yii::app()->baseUrl.'/winventory/warehouse/inventory';?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Inventori Gudang
                    </a>

                    <b class="arrow"></b>
                </li>
				<?php endif;?>
				<?php if (Yii::app()->user->hasAccess('transfer', 'default', 'index')):?>
                <li class="<?php echo $currentPage == 'transfer/default' ? "active" : "";?>">
                    <a href="<?php echo Yii::app()->baseUrl."/transfer/";?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Mutasi Barang
                    </a>

                    <b class="arrow"></b>
                </li>
				<?php endif;?>
				<?php if (Yii::app()->user->hasAccess('winventory', 'stockadjust', 'index')):?>
				<li class="<?php echo $currentPage == 'winventory/stockadjust' ? "active" : "";?>">
                    <a href="<?php echo Yii::app()->baseUrl."/winventory/stockadjust";?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Penyesuaian Stok
                    </a>

                    <b class="arrow"></b>
                </li>
				<?php endif;?>
            </ul>
        </li>
		<?php endif;?>
		<?php if (Yii::app()->user->hasAccess('salespayment', 'default', 'index') ||
					Yii::app()->user->hasAccess('salesorder', 'default', 'index') ||
					Yii::app()->user->hasAccess('salesinvoice', 'default', 'index') ||
					Yii::app()->user->hasAccess('salesreturn', 'default', 'index')):?>
		<li class="<?php echo in_array($moduleName, array('salesorder', 'salesinvoice', 'salesreturn', 'salespayment')) ? "active open" : "";?>">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-shopping-cart"></i>
                <span class="menu-text">Penjualan</span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
				<?php if (Yii::app()->user->hasAccess('salesorder', 'default', 'index')):?>
                <li class="<?php echo $currentPage == 'salesorder/default' ? "active" : "";?>">
                    <a href="<?php echo Yii::app()->baseUrl."/salesorder/";?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Sales Order
                    </a>

                    <b class="arrow"></b>
                </li>
				<?php endif;?>
				<?php if (Yii::app()->user->hasAccess('salesinvoice', 'default', 'index')):?>
                <li class="<?php echo $currentPage == 'salesinvoice/default' ? "active" : "";?>">
                    <a href="<?php echo Yii::app()->baseUrl."/salesinvoice/";?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Nota
                    </a>

                    <b class="arrow"></b>
                </li>
				<?php endif;?>
				<?php if (Yii::app()->user->hasAccess('salesreturn', 'default', 'index')):?>
				<li class="<?php echo $currentPage == 'salesreturn/default' ? "active" : "";?>">
                    <a href="<?php echo Yii::app()->baseUrl."/salesreturn/";?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Retur Penjualan
                    </a>

                    <b class="arrow"></b>
                </li>
				<?php endif;?>
				<?php if (Yii::app()->user->hasAccess('salespayment', 'default', 'index')):?>
				<li class="<?php echo $currentPage == 'salespayment/default' ? "active" : "";?>">
                    <a href="<?php echo Yii::app()->baseUrl."/salespayment/";?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Penerimaan Pembayaran
                    </a>

                    <b class="arrow"></b>
                </li>
				<?php endif;?>
            </ul>
        </li>
		<?php endif;?>
		<?php if (Yii::app()->user->hasAccess('purchaseorder', 'default', 'index') ||
					Yii::app()->user->hasAccess('purchasereturn', 'default', 'index') ||
					Yii::app()->user->hasAccess('purchasepayment', 'default', 'index')):?>
        <li class="<?php echo in_array($moduleName, array('purchaseorder', 'purchasereturn', 'purchasepayment')) ? "active open" : "";?>">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-credit-card"></i>
                <span class="menu-text">Pembelian</span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
				<?php if (Yii::app()->user->hasAccess('purchaseorder', 'default', 'index')):?>
                <li class="<?php echo $currentPage == 'purchaseorder/default' ? "active" : "";?>">
                    <a href="<?php echo Yii::app()->baseUrl."/purchaseorder/";?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Purchase Order
                    </a>

                    <b class="arrow"></b>
                </li>
				<?php endif;?>
				<?php if (Yii::app()->user->hasAccess('purchasereturn', 'default', 'index')):?>
                <li class="<?php echo $currentPage == 'purchasereturn/default' ? "active" : "";?>">
                    <a href="<?php echo Yii::app()->baseUrl."/purchasereturn/";?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Retur Pembelian
                    </a>

                    <b class="arrow"></b>
                </li>
				<?php endif;?>
				<?php if (Yii::app()->user->hasAccess('purchasepayment', 'default', 'index')):?>
				<li class="<?php echo $currentPage == 'purchasepayment/default' ? "active" : "";?>">
                    <a href="<?php echo Yii::app()->baseUrl."/purchasepayment/";?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Pembayaran Pembelian
                    </a>

                    <b class="arrow"></b>
                </li>
				<?php endif;?>
            </ul>
        </li>
		<?php endif;?>
		<?php if (Yii::app()->user->hasAccess('report', 'sales', 'salesorder') ||
					Yii::app()->user->hasAccess('report', 'sales', 'salesinvoice') ||
					Yii::app()->user->hasAccess('report', 'sales', 'salesreturn') ||
					Yii::app()->user->hasAccess('report', 'sales', 'salespayment') ||
					Yii::app()->user->hasAccess('report', 'purchase', 'purchaseorder') ||
					Yii::app()->user->hasAccess('report', 'purchase', 'purchasereturn') ||
					Yii::app()->user->hasAccess('report', 'purchase', 'purchasepayment') ||
					Yii::app()->user->hasAccess('report', 'inventory', 'transfer') ||
					Yii::app()->user->hasAccess('report', 'inventory', 'stockcard')):?>
        <li class="<?php echo $moduleName == 'report' ? "active open" : "";?>">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-desktop"></i>
                <span class="menu-text"> Laporan </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">
				<?php if (Yii::app()->user->hasAccess('report', 'sales', 'salesorder') ||
							Yii::app()->user->hasAccess('report', 'sales', 'salesinvoice') ||
							Yii::app()->user->hasAccess('report', 'sales', 'salesreturn') ||
							Yii::app()->user->hasAccess('report', 'sales', 'salespayment')):?>
                <li class="<?php echo $moduleName == 'report' && $currentController == 'sales' ? "active open" : "";?>">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Penjualan
                        <b class="arrow fa fa-angle-down"></b>
                    </a>
                    <b class="arrow"></b>
                    <ul class="submenu">
						<?php if (Yii::app()->user->hasAccess('report', 'sales', 'salesorder')):?>
                        <li class="<?php echo $moduleName == 'report' && $currentController == 'sales' && $currentFunction == 'salesorder' ? "active" : "";?>">
                            <a href="<?php echo Yii::app()->baseUrl."/report/sales/salesorder/";?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Laporan Sales Order
                            </a>
                            <b class="arrow"></b>
                        </li>
						<?php endif;?>
						<?php if (Yii::app()->user->hasAccess('report', 'sales', 'salesinvoice')):?>
                        <li class="<?php echo $moduleName == 'report' && $currentController == 'sales' && $currentFunction == 'salesinvoice' ? "active" : "";?>">
                            <a href="<?php echo Yii::app()->baseUrl."/report/sales/salesinvoice/";?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Laporan Penjualan
                            </a>
                            <b class="arrow"></b>
                        </li>
						<?php endif;?>
						<?php if (Yii::app()->user->hasAccess('report', 'sales', 'salesreturn')):?>
						<li class="<?php echo $moduleName == 'report' && $currentController == 'sales' && $currentFunction == 'salesreturn' ? "active" : "";?>">
                            <a href="<?php echo Yii::app()->baseUrl."/report/sales/salesreturn/";?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Laporan Retur Penjualan
                            </a>
                            <b class="arrow"></b>
                        </li>
						<?php endif;?>
						<?php if (Yii::app()->user->hasAccess('report', 'sales', 'salespayment')):?>
                        <li class="<?php echo $moduleName == 'report' && $currentController == 'sales' && $currentFunction == 'salespayment' ? "active" : "";?>">
							<a href="<?php echo Yii::app()->baseUrl."/report/sales/salespayment/";?>">
								<i class="menu-icon fa fa-caret-right"></i>
                                Laporan Pembayaran Nota
                            </a>
                            <b class="arrow"></b>
                        </li>
						<?php endif;?>
                    </ul>
                </li>
				<?php endif;?>
				<?php if (Yii::app()->user->hasAccess('report', 'purchase', 'purchaseorder') ||
							Yii::app()->user->hasAccess('report', 'purchase', 'purchasereturn') ||
							Yii::app()->user->hasAccess('report', 'purchase', 'purchasepayment')):?>
                <li class="<?php echo $moduleName == 'report' && $currentController == 'purchase' ? "active open" : "";?>">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Pembelian
                        <b class="arrow fa fa-angle-down"></b>
                    </a>
                    <b class="arrow"></b>
                    <ul class="submenu">
						<?php if (Yii::app()->user->hasAccess('report', 'purchase', 'purchaseorder')):?>
                        <li class="<?php echo $moduleName == 'report' && $currentController == 'purchase' && $currentFunction == 'purchaseorder' ? "active" : "";?>">
                            <a href="<?php echo Yii::app()->baseUrl."/report/purchase/purchaseorder/";?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Laporan Pembelian
                            </a>
                            <b class="arrow"></b>
                        </li>
						<?php endif;?>
						<?php if (Yii::app()->user->hasAccess('report', 'purchase', 'purchasereturn')):?>
                        <li class="<?php echo $moduleName == 'report' && $currentController == 'purchase' && $currentFunction == 'purchasereturn' ? "active" : "";?>">
                            <a href="<?php echo Yii::app()->baseUrl."/report/purchase/purchasereturn/";?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Laporan Retur Pembelian
                            </a>
                            <b class="arrow"></b>
                        </li>
						<?php endif;?>
						<?php if (Yii::app()->user->hasAccess('report', 'purchase', 'purchasepayment')):?>
                        <li class="<?php echo $moduleName == 'report' && $currentController == 'purchase' && $currentFunction == 'purchasepayment' ? "active" : "";?>">
							<a href="<?php echo Yii::app()->baseUrl."/report/purchase/purchasepayment/";?>">
								<i class="menu-icon fa fa-caret-right"></i>
                                Laporan Pembayaran Pembelian
                            </a>
                            <b class="arrow"></b>
                        </li>
						<?php endif;?>
                    </ul>
                </li>
				<?php endif;?>
				<?php if (Yii::app()->user->hasAccess('report', 'inventory', 'transfer') ||
							Yii::app()->user->hasAccess('report', 'inventory', 'stockcard')):?>
				<li class="<?php echo $moduleName == 'report' && $currentController == 'inventory' ? "active open" : "";?>">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Inventori
                        <b class="arrow fa fa-angle-down"></b>
                    </a>
                    <b class="arrow"></b>
                    <ul class="submenu">
						<?php if (Yii::app()->user->hasAccess('report', 'inventory', 'transfer')):?>
                        <li class="<?php echo $moduleName == 'report' && $currentController == 'inventory' && $currentFunction == 'transfer' ? "active" : "";?>">
                            <a href="<?php echo Yii::app()->baseUrl."/report/inventory/transfer/";?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Laporan Mutasi Barang
                            </a>
                            <b class="arrow"></b>
                        </li>
						<?php endif;?>
						<?php if (Yii::app()->user->hasAccess('report', 'inventory', 'stockcard')):?>
                        <li class="<?php echo $moduleName == 'report' && $currentController == 'inventory' && $currentFunction == 'stockcard' ? "active" : "";?>">
                            <a href="<?php echo Yii::app()->baseUrl."/report/inventory/stockcard/";?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Laporan Kartu Stok
                            </a>
                            <b class="arrow"></b>
                        </li>
						<?php endif;?>
                    </ul>
                </li>
				<?php endif;?>
			</ul>
		</li>
		<?php endif;?>
    </ul><!-- /.nav-list -->

    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>

    <div class="sidebar-toggle sidebar-expand" id="sidebar-expand">
        <i class="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-right" data-icon2="ace-icon fa fa-angle-double-left"></i>
    </div>

    <script type="text/javascript">
            try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
    </script>
</div>