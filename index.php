<?php

// change the following paths if necessary
$yii=dirname(__FILE__).'/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

// ($yii);
// Yii::createWebApplication($config)->run();
//
// The following lines are required to read all modules automatically.
// Reference: http://www.yiiframework.com/forum/index.php/topic/23467-dynamically-load-modules-models-and-configurations/page__p__114254__hl__module+.model+#entry114254
// by Jonathan (jonathan@vi8e.com)
//
require_once('incapsula.php');
require_once($yii);
class ExtendAbleWebApp extends CWebApplication
{

	protected function init()
	{
		// this example dynamically loads every module which can be found
		// under `modules` directory
		// this can be easily done to load modules
		// based on MySQL db or any other as well
		foreach (glob(dirname(__FILE__) . '/protected/modules/*', GLOB_ONLYDIR) as $moduleDirectory) {
			$this->setModules(array(basename($moduleDirectory)));
		}

		return parent::init();
	}

}

$app = new ExtendAbleWebApp($config);
$app->run();
